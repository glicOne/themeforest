msgid ""
msgstr ""
"Project-Id-Version: Pulchellus\n"
"POT-Creation-Date: 2013-04-07 20:51+0200\n"
"PO-Revision-Date: 2013-04-07 20:51+0200\n"
"Last-Translator: \n"
"Language-Team: Different Themes <support@different-themes.com>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.4\n"
"X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SearchPath-0: .\n"

#: 404.php:9
msgid "404"
msgstr ""

#: 404.php:10
msgid "Oops, it appears you've find an error."
msgstr ""

#: 404.php:11
msgid "Page you were looking for doesn't exist."
msgstr ""

#: 404.php:12
msgid "Back to home"
msgstr ""

#: category.php:63 search.php:63 includes/news.php:66
msgid "Read more"
msgstr ""

#: category.php:70 search.php:70 functions/homepage-blocks.php:116
#: functions/homepage-blocks.php:205 includes/news.php:73
msgid "No posts were found"
msgstr ""

#: comments.php:6
msgid "This post is password protected. Enter the password to view comments."
msgstr ""

#: comments.php:17
msgid " Comment responses"
msgstr ""

#: comments.php:27
msgid "No comments yet."
msgstr ""

#: comments.php:28
msgid "No one have left a comment for this post yet!"
msgstr ""

#: comments.php:37
#, php-format
msgid "Only <a href=\"%1$s\"> registered </a> users can comment."
msgstr ""

#: comments.php:44
msgid "Comment:"
msgstr ""

#: comments.php:44
msgid "Your comment.."
msgstr ""

#: comments.php:53
msgid "Send"
msgstr ""

#: footer.php:122
msgid "Email:"
msgstr ""

#: header.php:42
#, php-format
msgid "%s latest posts"
msgstr ""

#: header.php:43
#, php-format
msgid "%s latest comments"
msgstr ""

#: searchform.php:3
msgid "Type and press enter"
msgstr ""

#: template-contact.php:39
msgid "Attention! Please correct the errors below and try again."
msgstr ""

#: template-contact.php:41
msgid "Your name is <strong>required</strong>."
msgstr ""

#: template-contact.php:42
msgid "Your e-mail address is <strong>required</strong>."
msgstr ""

#: template-contact.php:43
msgid "You must <strong>enter a message</strong> to send."
msgstr ""

#: template-contact.php:46
msgid "Email sent successfully!"
msgstr ""

#: template-contact.php:53
msgid "Your name"
msgstr ""

#: template-contact.php:58
msgid "Your e-mail adress"
msgstr ""

#: template-contact.php:63
msgid "Subject"
msgstr ""

#: template-contact.php:76
msgid "Message"
msgstr ""

#: template-contact.php:81
msgid "Submit"
msgstr ""

#: template-contact.php:97 template-full-width.php:20
#: includes/news-single.php:77 includes/page-single.php:50
#: includes/portfolio-single.php:35
msgid "Sorry, no posts matched your criteria."
msgstr ""

#: template-portfolio-3.php:26 template-portfolio-4.php:25
msgid "All"
msgstr ""

#: template-portfolio-3.php:74 template-portfolio-4.php:74
msgid "No items were found"
msgstr ""

#: functions/ajax.php:271
msgid "From"
msgstr ""

#: functions/ajax.php:271
msgid "Contact Page"
msgstr ""

#: functions/ajax.php:284
msgid "Message:"
msgstr ""

#: functions/ajax.php:286 functions/filters.php:146
msgid "Name:"
msgstr ""

#: functions/ajax.php:287 functions/filters.php:147
msgid "E-mail:"
msgstr ""

#: functions/ajax.php:288
msgid "IP Address:"
msgstr ""

#: functions/breadcrumbs.php:5
msgid "Home"
msgstr ""

#: functions/breadcrumbs.php:96
msgid "Page"
msgstr ""

#: functions/filters.php:146
msgid "Nickname.."
msgstr ""

#: functions/filters.php:147
msgid "E-mail.."
msgstr ""

#: functions/filters.php:148
msgid "Website:"
msgstr ""

#: functions/filters.php:148
msgid "Website.."
msgstr ""

#: functions/filters.php:158
msgid "Leave a <strong>comment</strong>"
msgstr ""

#: functions/filters.php:185
msgid "By"
msgstr ""

#: functions/filters.php:185
msgid "All posts from"
msgstr ""

#: functions/homepage-blocks.php:90 functions/homepage-blocks.php:179
msgid "View all posts"
msgstr ""

#: functions/homepage-blocks.php:108 functions/homepage-blocks.php:197
msgid "0"
msgstr ""

#: functions/homepage-blocks.php:108 functions/homepage-blocks.php:197
msgid "1"
msgstr ""

#: functions/homepage-blocks.php:108 functions/homepage-blocks.php:197
msgid "%"
msgstr ""

#: functions/nav.php:54 functions/nav.php:56
msgid "Prev"
msgstr ""

#: functions/nav.php:123 functions/nav.php:125
msgid "Next"
msgstr ""

#: functions/nav.php:174
msgid "Previous"
msgstr ""

#: functions/other.php:221
msgid "Search Results For:"
msgstr ""

#: functions/other.php:227
msgid "Category"
msgstr ""

#: functions/other.php:230
msgid "Posts From"
msgstr ""

#: functions/other.php:235
msgid "Archive"
msgstr ""

#: functions/other.php:611
msgid "Avatar"
msgstr ""

#: functions/other.php:615
#, php-format
msgid "%1$s"
msgstr ""

#: functions/other.php:619
msgid "Your comment is awaiting moderation."
msgstr ""

#: functions/other.php:623
msgid "Reply"
msgstr ""

#: functions/register.php:15
msgid "Top Menu"
msgstr ""

#: functions/register.php:16
msgid "Footer Menu (Tags)"
msgstr ""

#: functions/register.php:27
msgid "Add New Item"
msgstr ""

#: functions/register.php:28
msgid "Edit Item"
msgstr ""

#: functions/register.php:29
msgid "New Portfolio Item"
msgstr ""

#: functions/register.php:30
msgid "View Item"
msgstr ""

#: functions/register.php:31
msgid "Search portfolio Items"
msgstr ""

#: functions/register.php:32
msgid "No portfolio items found"
msgstr ""

#: functions/register.php:33
msgid "No portfolio items found in Trash"
msgstr ""

#: includes/news-single.php:59
msgid "Tagged"
msgstr ""

#: includes/theme-config.php:622
msgid "Select A List"
msgstr ""

#: includes/theme-config.php:632
msgid ""
"This AWeber account doesn't currently have any lists or your authorization "
"code is incorrect!"
msgstr ""

#: includes/theme-config.php:947
msgid "You didn't create a slider yet."
msgstr ""

#: includes/admin/functions/default-values.php:88
#: includes/lib/class-tgm-plugin-activation.php:164
msgid "Install Required Plugins"
msgstr ""

#: includes/admin/functions/default-values.php:89
#: includes/lib/class-tgm-plugin-activation.php:165
msgid "Install Plugins"
msgstr ""

#: includes/admin/functions/default-values.php:90
#: includes/lib/class-tgm-plugin-activation.php:166
#, php-format
msgid "Installing Plugin: %s"
msgstr ""

#: includes/admin/functions/default-values.php:91
msgid "Something went wrong with the plugin API."
msgstr ""

#: includes/admin/functions/default-values.php:102
#: includes/lib/class-tgm-plugin-activation.php:178
#: includes/lib/class-tgm-plugin-activation.php:490
msgid "Return to Required Plugins Installer"
msgstr ""

#: includes/admin/functions/default-values.php:103
#: includes/lib/class-tgm-plugin-activation.php:179
#: includes/lib/class-tgm-plugin-activation.php:1846
msgid "Plugin activated successfully."
msgstr ""

#: includes/admin/functions/default-values.php:104
#, php-format
msgid "All plugins installed and activated successfully. %s"
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:167
msgid "Something went wrong."
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:180
#, php-format
msgid "All plugins installed and activated successfully. %1$s"
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:517
#: includes/lib/class-tgm-plugin-activation.php:2058
msgid "Return to the Dashboard"
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:545
#, php-format
msgid "The following plugin was activated successfully: %s."
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:704
msgid "Dismiss this notice"
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:1091
msgid "External Link"
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:1096
msgid "Private Repository"
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:1099
msgid "Pre-Packaged"
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:1103
msgid "WordPress Repository"
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:1106
msgid "Required"
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:1106
msgid "Recommended"
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:1109
msgid "Not Installed"
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:1111
msgid "Installed But Not Activated"
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:1281
#, php-format
msgid ""
"No plugins to install or activate. <a href=\"%1$s\" title=\"Return to the "
"Dashboard\">Return to the Dashboard</a>"
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:1297
msgid "Plugin"
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:1298
msgid "Source"
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:1299
msgid "Type"
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:1300
msgid "Status"
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:1318
msgid "Install"
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:1319
msgid "Activate"
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:1829
msgid "Install package not available."
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:1830
#, php-format
msgid "Downloading install package from <span class=\"code\">%s</span>&#8230;"
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:1831
msgid "Unpacking the package&#8230;"
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:1832
msgid "Installing the plugin&#8230;"
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:1833
msgid "Plugin install failed."
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:1834
msgid "Plugin installed successfully."
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:1845
msgid "Plugin activation failed."
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:1955
msgid ""
"The installation and activation process is starting. This process may take a "
"while on some hosts, so please be patient."
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:1956
#, php-format
msgid "%1$s installed and activated successfully."
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:1956
#: includes/lib/class-tgm-plugin-activation.php:1965
msgid "Show Details"
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:1956
#: includes/lib/class-tgm-plugin-activation.php:1965
msgid "Hide Details"
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:1957
msgid "All installations and activations have been completed."
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:1958
#, php-format
msgid "Installing and Activating Plugin %1$s (%2$d/%3$d)"
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:1962
msgid ""
"The installation process is starting. This process may take a while on some "
"hosts, so please be patient."
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:1963
#, php-format
msgid "An error occurred while installing %1$s: <strong>%2$s</strong>."
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:1964
#, php-format
msgid "The installation of %1$s failed."
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:1965
#, php-format
msgid "%1$s installed successfully."
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:1966
msgid "All installations have been completed."
msgstr ""

#: includes/lib/class-tgm-plugin-activation.php:1967
#, php-format
msgid "Installing Plugin %1$s (%2$d/%3$d)"
msgstr ""

#: includes/widgets/widget-pulchellus-flickr.php:15
#: includes/widgets/widget-pulchellus-latest-comments.php:14
#: includes/widgets/widget-pulchellus-latest-posts-cat.php:14
#: includes/widgets/widget-pulchellus-latest-posts.php:14
#: includes/widgets/widget-pulchellus-social.php:13
#: includes/widgets/widget-pulchellus-tags.php:14
#: includes/widgets/widget-pulchellus-text.php:14
msgid "Title:"
msgstr ""

#: includes/widgets/widget-pulchellus-flickr.php:18
msgid "Flickr ID:"
msgstr ""

#: includes/widgets/widget-pulchellus-flickr.php:19
#: includes/widgets/widget-pulchellus-latest-posts-cat.php:17
#: includes/widgets/widget-pulchellus-tags.php:15
msgid "Count:"
msgstr ""

#: includes/widgets/widget-pulchellus-latest-comments.php:17
#: includes/widgets/widget-pulchellus-popular-comments.php:17
msgid "Comment count:"
msgstr ""

#: includes/widgets/widget-pulchellus-latest-posts-cat.php:79
#: includes/widgets/widget-pulchellus-latest-posts.php:64
#: includes/widgets/widget-pulchellus-popular-comments.php:116
msgid "No posts where found"
msgstr ""

#: includes/widgets/widget-pulchellus-latest-posts.php:17
#: includes/widgets/widget-pulchellus-popular-comments.php:16
msgid "Post count:"
msgstr ""

#: includes/widgets/widget-pulchellus-popular-comments.php:15
#: includes/widgets/widget-pulchellus-toggles.php:15
msgid "Widget Title:"
msgstr ""

#: includes/widgets/widget-pulchellus-popular-comments.php:46
msgid "Latest Comments"
msgstr ""

#: includes/widgets/widget-pulchellus-popular-comments.php:47
msgid "Popular Posts"
msgstr ""

#: includes/widgets/widget-pulchellus-social.php:23
msgid "Account Url To "
msgstr ""

#: includes/widgets/widget-pulchellus-text.php:17
#: includes/widgets/widget-pulchellus-toggles.php:19
msgid "Text:"
msgstr ""

#: includes/widgets/widget-pulchellus-toggles.php:16
msgid "Toggle Title:"
msgstr ""

#: lib/js/scripts.php:13
msgid "You didn't enter Your First Name."
msgstr ""

#: lib/js/scripts.php:15
msgid "First Name is the wrong length."
msgstr ""

#: lib/js/scripts.php:26
msgid "You didn't enter an email address."
msgstr ""

#: lib/js/scripts.php:28
msgid "The email address contains illegal characters."
msgstr ""

#: lib/js/scripts.php:40
msgid "You didn't enter Your message."
msgstr ""

#: lib/js/scripts.php:42
msgid "The message is to short."
msgstr ""
