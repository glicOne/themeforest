#, fuzzy
msgid ""
msgstr ""
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"Project-Id-Version: Renovation Progression\n"
"POT-Creation-Date: 2016-04-28 10:27-0700\n"
"PO-Revision-Date: 2016-04-28 10:27-0700\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.7\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-WPHeader: style.css\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;"
"_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"

#: 404.php:14
msgid "Oops! That page can&rsquo;t be found."
msgstr ""

#: 404.php:23
msgid "It looks like nothing was found at this location. Maybe try and search?"
msgstr ""

#: archive-portfolio.php:17
msgid "Portfolio"
msgstr ""

#: archive-service.php:17
msgid "Services"
msgstr ""

#: archive-testimonial.php:17
msgid "Testimonials"
msgstr ""

#: archive.php:29
#, php-format
msgid "Author: %s"
msgstr ""

#: archive.php:37
#, php-format
msgid "Day: %s"
msgstr ""

#: archive.php:40
#, php-format
msgid "Month: %s"
msgstr ""

#: archive.php:43
#, php-format
msgid "Year: %s"
msgstr ""

#: archive.php:46
msgid "Asides"
msgstr ""

#: archive.php:49
msgid "Images"
msgstr ""

#: archive.php:52
msgid "Videos"
msgstr ""

#: archive.php:55
msgid "Quotes"
msgstr ""

#: archive.php:58
msgid "Links"
msgstr ""

#: archive.php:61
msgid "Archives"
msgstr ""

#: comments.php:29
#, php-format
msgctxt "comments title"
msgid "One comment on &ldquo;%2$s&rdquo;"
msgid_plural "%1$s comments on &ldquo;%2$s&rdquo;"
msgstr[0] ""
msgstr[1] ""

#: comments.php:36 comments.php:56
msgid "Comment navigation"
msgstr ""

#: comments.php:37 comments.php:57
msgid "&larr; Older Comments"
msgstr ""

#: comments.php:38 comments.php:58
msgid "Newer Comments &rarr;"
msgstr ""

#: comments.php:68
msgid "Comments are closed."
msgstr ""

#: content-blog-home.php:64 content-single.php:60 content.php:64
msgid "By"
msgstr ""

#: content-blog-home.php:78 content.php:78
msgid "Continue reading <span class=\"meta-nav\">&rarr;</span>"
msgstr ""

#: content-blog-home.php:82 content-page.php:14 content-single.php:73
#: content.php:82 image.php:65
msgid "Pages:"
msgstr ""

#: content-page.php:18 content-single.php:77 image.php:39
#: inc/template-tags.php:100 inc/template-tags.php:128
#: inc/template-tags.php:170 inc/template-tags.php:198
msgid "Edit"
msgstr ""

#: content-single-portfolio.php:75
msgid "Back to Portfolio"
msgstr ""

#: content-single.php:60 content.php:64
msgid "No Comments"
msgstr ""

#: content-single.php:60 content.php:64
msgctxt "comments number"
msgid "1 Comment"
msgstr ""

#: content-single.php:60 content.php:64
msgctxt "comments number"
msgid "% Comments"
msgstr ""

#: functions.php:69
msgid "Primary Left Menu"
msgstr ""

#: functions.php:70
msgid "Primary Right Menu"
msgstr ""

#: functions.php:176
msgid "Sidebar"
msgstr ""

#: functions.php:186
msgid "Footer Widgets"
msgstr ""

#: functions.php:187
msgid "Footer widgets"
msgstr ""

#: functions.php:196
msgid "Homepage Widgets"
msgstr ""

#: functions.php:206
msgid "Home: Widgets on all Pages"
msgstr ""

#: functions.php:216
msgid "Portfolio Sidebar"
msgstr ""

#: functions.php:226
msgid "Services Sidebar"
msgstr ""

#: functions.php:236
msgid "Shop Sidebar"
msgstr ""

#: functions.php:343
msgid "Navigate to..."
msgstr ""

#: image.php:29
#, php-format
msgid ""
"Published <span class=\"entry-date\"><time class=\"entry-date\" datetime="
"\"%1$s\">%2$s</time></span> at <a href=\"%3$s\">%4$s &times; %5$s</a> in <a "
"href=\"%6$s\" rel=\"gallery\">%7$s</a>"
msgstr ""

#: image.php:44
msgid "<span class=\"meta-nav\">&larr;</span> Previous"
msgstr ""

#: image.php:45
msgid "Next <span class=\"meta-nav\">&rarr;</span>"
msgstr ""

#: inc/customizer.php:50
msgid "Default"
msgstr ""

#: inc/customizer.php:66
msgid "Theme Settings"
msgstr ""

#: inc/customizer.php:67
msgid "Main Theme Settings"
msgstr ""

#: inc/customizer.php:85
msgid "Theme Logo"
msgstr ""

#: inc/customizer.php:104
msgid "Logo Width"
msgstr ""

#: inc/customizer.php:120
msgid "Align logo to the left instead of center"
msgstr ""

#: inc/customizer.php:138
msgid "Set Navigation Fixed?"
msgstr ""

#: inc/customizer.php:156
msgid "Copyright Text"
msgstr ""

#: inc/customizer.php:177
msgid "Footer Column Count (1-4)"
msgstr ""

#: inc/customizer.php:193
msgid "Display comments for pages?"
msgstr ""

#: inc/customizer.php:220
msgid "Portfolio posts per column (2-4)"
msgstr ""

#: inc/customizer.php:239
msgid "Portfolio posts per page"
msgstr ""

#: inc/customizer.php:258
msgid "Testimonial posts per column (2-4)"
msgstr ""

#: inc/customizer.php:277
msgid "Testimonial posts per page"
msgstr ""

#: inc/customizer.php:295
msgid "Services posts per column (2-4)"
msgstr ""

#: inc/customizer.php:314
msgid "Services posts per page"
msgstr ""

#: inc/customizer.php:332
msgid "Shop posts per column (2-4)"
msgstr ""

#: inc/customizer.php:347
msgid "Background Colors"
msgstr ""

#: inc/customizer.php:364
msgid "Header Background Color"
msgstr ""

#: inc/customizer.php:377
msgid "Navigation Border Bottom Color"
msgstr ""

#: inc/customizer.php:391
msgid "Navigation Hover Background Color"
msgstr ""

#: inc/customizer.php:405
msgid "Page Title Background Color"
msgstr ""

#: inc/customizer.php:419
msgid "Body Background Color"
msgstr ""

#: inc/customizer.php:437
msgid "Footer Widget Background Color"
msgstr ""

#: inc/customizer.php:450
msgid "Footer Background Color"
msgstr ""

#: inc/customizer.php:465
msgid "Button Background Color"
msgstr ""

#: inc/customizer.php:480
msgid "Button Hover Background Color"
msgstr ""

#: inc/customizer.php:496
msgid "Slider/Secondary Button Background"
msgstr ""

#: inc/customizer.php:509
msgid "Slider/Secondary Hover Button Background"
msgstr ""

#: inc/customizer.php:523
msgid "Pagination Highlight Background"
msgstr ""

#: inc/customizer.php:537
msgid "Pagination Highlight Border"
msgstr ""

#: inc/customizer.php:550
msgid "Font Colors"
msgstr ""

#: inc/customizer.php:563
msgid "Body Font Color"
msgstr ""

#: inc/customizer.php:577
msgid "Page Title Font Color"
msgstr ""

#: inc/customizer.php:592
msgid "Main Link Color"
msgstr ""

#: inc/customizer.php:606
msgid "Main Hover Link Color"
msgstr ""

#: inc/customizer.php:619
msgid "Navigation Link Color"
msgstr ""

#: inc/customizer.php:634
msgid "Navigation Current/Hover Color"
msgstr ""

#: inc/customizer.php:648
msgid "Headings Font Color"
msgstr ""

#: inc/customizer.php:662
msgid "Button Font Color"
msgstr ""

#: inc/customizer.php:676
msgid "Button Hover Font Color"
msgstr ""

#: inc/customizer.php:690
msgid "Slider/Secondary Button Font Color"
msgstr ""

#: inc/customizer.php:704
msgid "Slider/Secondary Button Hover Font Color"
msgstr ""

#: inc/extras.php:66
#, php-format
msgid "Page %s"
msgstr ""

#: inc/template-tags.php:26
msgid "&lsaquo; Previous"
msgstr ""

#: inc/template-tags.php:27
msgid "Next &rsaquo;"
msgstr ""

#: inc/template-tags.php:60
msgid "Post navigation"
msgstr ""

#: inc/template-tags.php:64
msgid "<span>&larr;</span> Previous Article"
msgstr ""

#: inc/template-tags.php:65
msgid "Next Article <span>&rarr;</span>"
msgstr ""

#: inc/template-tags.php:73
msgid "<span class=\"meta-nav\">&larr;</span> Older posts"
msgstr ""

#: inc/template-tags.php:77
msgid "Newer posts <span class=\"meta-nav\">&rarr;</span>"
msgstr ""

#: inc/template-tags.php:100 inc/template-tags.php:170
msgid "Pingback:"
msgstr ""

#: inc/template-tags.php:118 inc/template-tags.php:124
#: inc/template-tags.php:188 inc/template-tags.php:194
#, php-format
msgid "%1$s %2$s"
msgstr ""

#: inc/template-tags.php:132
msgid "Your comment is awaiting moderation."
msgstr ""

#: inc/template-tags.php:202
msgid "Your review is awaiting moderation."
msgstr ""

#: inc/template-tags.php:284
#, php-format
msgid "<time class=\"entry-date\" datetime=\"%3$s\">%4$s</time>"
msgstr ""

#: inc/template-tags.php:290
#, php-format
msgid "View all posts by %s"
msgstr ""

#: inc/template-tags.php:360 inc/template-tags.php:391
#: inc/template-tags.php:422
msgid "All"
msgstr ""

#: index.php:39
msgid "Latest Posts"
msgstr ""

#: no-results.php:13
msgid "Nothing Found"
msgstr ""

#: no-results.php:14 search.php:15
msgid "You are here:"
msgstr ""

#: no-results.php:31
#, php-format
msgid ""
"Ready to publish your first post? <a href=\"%1$s\">Get started here</a>."
msgstr ""

#: no-results.php:35
msgid ""
"Sorry, but nothing matched your search terms. Please try again with some "
"different keywords."
msgstr ""

#: no-results.php:40
msgid ""
"It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps "
"searching can help."
msgstr ""

#: related-portfolio-posts.php:30
msgid "Related Projects"
msgstr ""

#: search.php:14
#, php-format
msgid "Search Results for: %s"
msgstr ""

#: searchform.php:10
msgctxt "label"
msgid "Search for:"
msgstr ""

#: searchform.php:11
msgctxt "placeholder"
msgid "Start searching&hellip;"
msgstr ""

#: searchform.php:13
msgctxt "submit button"
msgid "Search"
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:161
#: tgm-plugin-activation/plugin-activation.php:115
msgid "Install Required Plugins"
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:162
#: tgm-plugin-activation/plugin-activation.php:116
msgid "Install Plugins"
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:163
#: tgm-plugin-activation/plugin-activation.php:117
#, php-format
msgid "Installing Plugin: %s"
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:164
msgid "Something went wrong."
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:165
#: tgm-plugin-activation/plugin-activation.php:119
#, php-format
msgid "This theme requires the following plugin: %1$s."
msgid_plural "This theme requires the following plugins: %1$s."
msgstr[0] ""
msgstr[1] ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:166
#: tgm-plugin-activation/plugin-activation.php:120
#, php-format
msgid "This theme recommends the following plugin: %1$s."
msgid_plural "This theme recommends the following plugins: %1$s."
msgstr[0] ""
msgstr[1] ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:167
#: tgm-plugin-activation/plugin-activation.php:121
#, php-format
msgid ""
"Sorry, but you do not have the correct permissions to install the %s plugin. "
"Contact the administrator of this site for help on getting the plugin "
"installed."
msgid_plural ""
"Sorry, but you do not have the correct permissions to install the %s "
"plugins. Contact the administrator of this site for help on getting the "
"plugins installed."
msgstr[0] ""
msgstr[1] ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:168
#: tgm-plugin-activation/plugin-activation.php:122
#, php-format
msgid "The following required plugin is currently inactive: %1$s."
msgid_plural "The following required plugins are currently inactive: %1$s."
msgstr[0] ""
msgstr[1] ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:169
#: tgm-plugin-activation/plugin-activation.php:123
#, php-format
msgid "The following recommended plugin is currently inactive: %1$s."
msgid_plural "The following recommended plugins are currently inactive: %1$s."
msgstr[0] ""
msgstr[1] ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:170
#: tgm-plugin-activation/plugin-activation.php:124
#, php-format
msgid ""
"Sorry, but you do not have the correct permissions to activate the %s "
"plugin. Contact the administrator of this site for help on getting the "
"plugin activated."
msgid_plural ""
"Sorry, but you do not have the correct permissions to activate the %s "
"plugins. Contact the administrator of this site for help on getting the "
"plugins activated."
msgstr[0] ""
msgstr[1] ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:171
#: tgm-plugin-activation/plugin-activation.php:125
#, php-format
msgid ""
"The following plugin needs to be updated to its latest version to ensure "
"maximum compatibility with this theme: %1$s."
msgid_plural ""
"The following plugins need to be updated to their latest version to ensure "
"maximum compatibility with this theme: %1$s."
msgstr[0] ""
msgstr[1] ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:172
#: tgm-plugin-activation/plugin-activation.php:126
#, php-format
msgid ""
"Sorry, but you do not have the correct permissions to update the %s plugin. "
"Contact the administrator of this site for help on getting the plugin "
"updated."
msgid_plural ""
"Sorry, but you do not have the correct permissions to update the %s plugins. "
"Contact the administrator of this site for help on getting the plugins "
"updated."
msgstr[0] ""
msgstr[1] ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:173
#: tgm-plugin-activation/plugin-activation.php:127
msgid "Begin installing plugin"
msgid_plural "Begin installing plugins"
msgstr[0] ""
msgstr[1] ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:174
#: tgm-plugin-activation/plugin-activation.php:128
msgid "Begin activating plugin"
msgid_plural "Begin activating plugins"
msgstr[0] ""
msgstr[1] ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:175
#: tgm-plugin-activation/plugin-activation.php:129
msgid "Return to Required Plugins Installer"
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:176
msgid "Return to the dashboard"
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:177
#: tgm-plugin-activation/class-tgm-plugin-activation.php:1956
#: tgm-plugin-activation/plugin-activation.php:130
msgid "Plugin activated successfully."
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:178
#: tgm-plugin-activation/class-tgm-plugin-activation.php:1644
msgid "The following plugin was activated successfully:"
msgid_plural "The following plugins were activated successfully:"
msgstr[0] ""
msgstr[1] ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:179
#, php-format
msgid "All plugins installed and activated successfully. %1$s"
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:180
msgid "Dismiss this notice"
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:532
#: tgm-plugin-activation/class-tgm-plugin-activation.php:2173
msgid "Return to the Dashboard"
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:1167
#: tgm-plugin-activation/class-tgm-plugin-activation.php:1348
msgid "Private Repository"
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:1170
#: tgm-plugin-activation/class-tgm-plugin-activation.php:1351
msgid "Pre-Packaged"
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:1175
msgid "WordPress Repository"
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:1178
msgid "Required"
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:1178
msgid "Recommended"
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:1181
msgid "Not Installed"
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:1183
msgid "Installed But Not Activated"
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:1288
#: tgm-plugin-activation/class-tgm-plugin-activation.php:1409
msgid "Install"
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:1312
#: tgm-plugin-activation/class-tgm-plugin-activation.php:1410
msgid "Activate"
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:1372
#, php-format
msgid ""
"No plugins to install or activate. <a href=\"%1$s\" title=\"Return to the "
"Dashboard\">Return to the Dashboard</a>"
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:1388
msgid "Plugin"
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:1389
msgid "Source"
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:1390
msgid "Type"
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:1391
msgid "Status"
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:1939
msgid "Install package not available."
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:1940
#, php-format
msgid "Downloading install package from <span class=\"code\">%s</span>&#8230;"
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:1941
msgid "Unpacking the package&#8230;"
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:1942
msgid "Installing the plugin&#8230;"
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:1943
msgid "Plugin install failed."
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:1944
msgid "Plugin installed successfully."
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:1955
msgid "Plugin activation failed."
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:2069
msgid ""
"The installation and activation process is starting. This process may take a "
"while on some hosts, so please be patient."
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:2070
#, php-format
msgid "%1$s installed and activated successfully."
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:2070
#: tgm-plugin-activation/class-tgm-plugin-activation.php:2079
msgid "Show Details"
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:2070
#: tgm-plugin-activation/class-tgm-plugin-activation.php:2079
msgid "Hide Details"
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:2071
msgid "All installations and activations have been completed."
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:2072
#, php-format
msgid "Installing and Activating Plugin %1$s (%2$d/%3$d)"
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:2076
msgid ""
"The installation process is starting. This process may take a while on some "
"hosts, so please be patient."
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:2077
#, php-format
msgid "An error occurred while installing %1$s: <strong>%2$s</strong>."
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:2078
#, php-format
msgid "The installation of %1$s failed."
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:2079
#, php-format
msgid "%1$s installed successfully."
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:2080
msgid "All installations have been completed."
msgstr ""

#: tgm-plugin-activation/class-tgm-plugin-activation.php:2081
#, php-format
msgid "Installing Plugin %1$s (%2$d/%3$d)"
msgstr ""

#: tgm-plugin-activation/plugin-activation.php:118
msgid "Something went wrong with the plugin API."
msgstr ""

#: tgm-plugin-activation/plugin-activation.php:131
#, php-format
msgid "All plugins installed and activated successfully. %s"
msgstr ""

#: widgets/flickr-widget.php:100 widgets/latest-blog-posts.php:155
#: widgets/latest-portfolio-posts.php:178 widgets/latest-service-posts.php:177
#: widgets/latest-testimonial-posts.php:177 widgets/single-highlight.php:128
msgid "Title"
msgstr ""

#: widgets/flickr-widget.php:105
msgid "Flickr ID: (Find from"
msgstr ""

#: widgets/flickr-widget.php:110
msgid "Number of flickr image to show"
msgstr ""

#: widgets/latest-blog-posts.php:160 widgets/latest-portfolio-posts.php:182
#: widgets/latest-service-posts.php:182
#: widgets/latest-testimonial-posts.php:182 widgets/single-highlight.php:134
msgid "Summary Text"
msgstr ""

#: widgets/latest-blog-posts.php:168
msgid "Filter by Category"
msgstr ""

#: widgets/latest-blog-posts.php:178
msgid "Number of posts"
msgstr ""

#: widgets/latest-blog-posts.php:182
msgid "Number of columns (1-4)"
msgstr ""

#: widgets/latest-blog-posts.php:187 widgets/latest-portfolio-posts.php:203
#: widgets/latest-service-posts.php:203
#: widgets/latest-testimonial-posts.php:203 widgets/single-highlight.php:141
msgid "Button Text"
msgstr ""

#: widgets/latest-blog-posts.php:192 widgets/latest-portfolio-posts.php:208
#: widgets/latest-service-posts.php:208
#: widgets/latest-testimonial-posts.php:208 widgets/single-highlight.php:146
msgid "Button Link"
msgstr ""

#: widgets/latest-blog-posts.php:197 widgets/latest-portfolio-posts.php:213
#: widgets/latest-service-posts.php:213
#: widgets/latest-testimonial-posts.php:213 widgets/single-highlight.php:151
msgid "Button Icon"
msgstr ""

#: widgets/latest-blog-posts.php:202 widgets/latest-portfolio-posts.php:218
#: widgets/latest-service-posts.php:218
#: widgets/latest-testimonial-posts.php:218 widgets/single-highlight.php:158
msgid "Widget Background Color"
msgstr ""

#: widgets/latest-blog-posts.php:210 widgets/latest-portfolio-posts.php:224
#: widgets/latest-service-posts.php:224
#: widgets/latest-testimonial-posts.php:224 widgets/single-highlight.php:166
msgid "Widget Background Image"
msgstr ""

#: widgets/latest-blog-posts.php:217 widgets/latest-portfolio-posts.php:230
#: widgets/latest-service-posts.php:230
#: widgets/latest-testimonial-posts.php:230 widgets/single-highlight.php:172
msgid "Add Image"
msgstr ""

#: widgets/latest-blog-posts.php:236
msgid "Check box for light heading"
msgstr ""

#: widgets/latest-portfolio-posts.php:190 widgets/latest-service-posts.php:190
#: widgets/latest-testimonial-posts.php:190
msgid "Number of Posts"
msgstr ""

#: widgets/latest-portfolio-posts.php:194 widgets/latest-service-posts.php:194
#: widgets/latest-testimonial-posts.php:194
msgid "Number of columns (2-4)"
msgstr ""

#: widgets/latest-portfolio-posts.php:198
msgid "Portfolio Category Slug<br>(Leave blank to pull all posts)"
msgstr ""

#: widgets/latest-portfolio-posts.php:250 widgets/latest-service-posts.php:250
#: widgets/latest-testimonial-posts.php:250
msgid "Check box for light heading text"
msgstr ""

#: widgets/latest-service-posts.php:198
msgid "Service Category Slug <br>(Leave blank to pull all posts)"
msgstr ""

#: widgets/latest-testimonial-posts.php:198
msgid "Testimonial Category Slug <br>(Leave blank to pull all posts)"
msgstr ""

#: widgets/single-highlight.php:191
msgid "Check box for light text color"
msgstr ""

#: widgets/social-widget.php:105
msgid "Title:"
msgstr ""

#: widgets/social-widget.php:112
msgid "Text Field"
msgstr ""

#: widgets/social-widget.php:119 widgets/social-widget.php:124
#: widgets/social-widget.php:129 widgets/social-widget.php:134
#: widgets/social-widget.php:139 widgets/social-widget.php:144
#: widgets/social-widget.php:149 widgets/social-widget.php:154
msgid "Link"
msgstr ""

#: widgets/social-widget.php:159
msgid "E-mail Address"
msgstr ""

#. Theme Name of the plugin/theme
msgid "Renovation Progression"
msgstr ""

#. Theme URI of the plugin/theme
msgid "http://progressionstudios.com/"
msgstr ""

#. Description of the plugin/theme
msgid "ThemeForest Premium Theme"
msgstr ""

#. Author of the plugin/theme
msgid "Progression Studios"
msgstr ""

#. Author URI of the plugin/theme
msgid "http://themeforest.net/user/ProgressionStudios"
msgstr ""
