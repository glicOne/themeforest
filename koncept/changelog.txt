*** Koncept Changelog ***

14 July 2016 - Version 1.7.4

	* Added support for the new Google Maps API
	* Updated the Revolution Slider to 5.2.6

17 June 2016 - Version 1.7.3

	* Added support for WooCommerce 2.6
	* Updated the Revolution Slider and Visual Composer plugins to their latest versions
	* Updated Font Awesome library to 4.6.3

25 April 2016 - Version 1.7.2

	* Updated the Revolution Slider and Visual Composer plugins to their latest versions

13 April 2016 - Version 1.7.1
	
	* Updated the Revolution Slider and Visual Composer plugins to their latest versions
	* Improved compatibility for WooCommerce 2.5.3

15 February 2016 - Version 1.7

	* Added SIDEKICK integration
	* Fixed an issue with WooCommerce product variation images
	* Improved compatibility for WooCommerce 2.5.2

30 January 2016 - Version 1.6.7
		
	* Added compatibility for WooCommerce 2.5.0
	* Updated Font Awesome to v4.5.0
	* Updated the Revolution Slider and Visual Composer plugins to their latest versions

12 December 2015 - Version 1.6.6

	* Fixed an issue related to pagination in WP4.4
	* Updated the Revolution Slider and Visual Composer plugins to their latest versions

30 October 2015 - Version 1.6.5

	* Fixed an issue related to WooCommerce cart images
	* Fixed the accordion shortcode
	* Fixed the single image shortcode

9 October 2015 - Version 1.6.4

	* Fixed an issue related to WooCommerce 2.4.7

7 October 2015 - Version 1.6.3

	* Updated the Revolution Slider and Visual Composer plugins to their latest versions
	* Added compatibility for WooCommerce 2.4.7

21 August 2015 - Version 1.6.2

	* Updated the Revolution Slider to v5.0.4.1, which includes support for WordPress 4.3

18 August 2015 - Version 1.6.1

	* Fixed the tabs shortcode issue

15 August 2015 - Version 1.6

	* Added support for WooCommerce 2.4
	* Updated the Revolution Slider to the all new version 5

7 July 2015 - Version 1.5.8

	* Added a fail safe against Envato API issues (fixed admin login problems)

3 July 2015 - Version 1.5.7

	* One more minor fix related to WooCommerce

2 July 2015 - Version 1.5.6

	* Critical fix after last update

2 July 2015 - Version 1.5.5

	* Fixed an error with WooCommerce headers
	* Fixed an issue with YouTube videos inside the slider plugin
	* Updated the Revolution Slider and Visual Composer plugins to their latest versions

25 April 2015 - Version 1.5.4
 
	* Improved theme security
	* Fixed broken cart page in WooCommerce 2.3.8

22 April 2015 - Version 1.5.3

	* Fixed some minor styling issues, mostly related to WooCommerce

11 March 2015 - Version 1.5.2

	* Added categories support in the shop grid
	* Improved portfolio layout handling
	* Fixed a SEO issue within the portfolio
	* Fixed more WooCommerce 2.3 issues

28 February 2015 - Version 1.5.1

	* Fixed an issue with infinite loading on retina devices

27 February 2015 - Version 1.5

	* Added a new blog style
	* Improved WooCommerce 2.3 support

24 February 2015 - Version 1.4.1

	* Fixed an issue with HTML code in page excerpts
	* Fixed an issue with the tabs shortcode
	* Improved WooCommerce 2.3 support

12 February 2015 - Version 1.4

	* Added support for WooCommerce 2.3
	* Removed default grid "checkboard" background
	* Fixed an issue related to the single images shortcode
	* Fixed a style issue in Windows Phones

21 January 2015 - Version 1.3.1

	* Fixed a few styling issues

10 January 2015 - Version 1.3

	* Added social meta tags in the header
	* Added twitter feed shortcode
	* Fixed some other minor bugs

20 December 2014 - Version 1.2.3

	* Added WP 4.1 compatibility
	* Fixed some bugs related to media formatting in blog posts
	* Updated the Revolution Slider and Visual Composer plugins to their latest versions

1 December 2014 - Version 1.2.2

	* Improved image scaling for the single images shortcode
	* Added alt tags for the single images shortcode
	* Fixed navigation styling issues in the Revolution Slider plugin
	* Updated the Revolution Slider plugin to it's latest version

25 November 2014 - Version 1.2.1

	* Fixed a bug with the content slider
	* Fixed a bug with the images slider caused by last update
	* Fixed some WooCommerce related errors
	* Fixed image scaling issues 
	* Added changelog view in the backend

19 November 2014 - Version 1.2

	* Added a new gallery page template	
	* Added RTL support
	* Added a header widget area
	* Added Google Fonts subsets in the Theme Customizer
	* Improved support for WPML 
	* Improved retina support
	* Updated the Revolution Slider plugin to it's latest version
	* Fixed some errors related to variable products in WooCommerce
	* Fixed an issue in the Visual Composer backend

10 November 2014 - Version 1.1

	* Added support for WooCommerce
	* Added a new portfolio masonry style - "simple resizing"
	* Added the ability to post content on the portfolio page

6 November 2014 - Version 1.0.4

	* Fixed a small bug related to SVG colors

5 November 2014 - Version 1.0.3

	* Fixed a bug related to the unlimited portfolios feature
	* Fixed some responsiveness issues
	* Fixed another bug related to the social shortcodes

31 October 2014 - Version 1.0.2
	
	* Fixed social shortcode issues
	* Fixed header margin issue
	* Improved IE compatibility

24 October 2014 - Version 1.0.1

	* Fixed some minor bugs
	* Added the option to disable unlimited portfolios
	* Added the option for a pure HTML header in pages

21 October 2014 - Version 1.0
	
	* First Release