*** Heartfelt Changelog ***
July 12 2016 - Version 2.6.2
	* Added support for Google Maps API key for contact page

June 30 2016 - Version 2.6.1
	* Compatibility for WooCommerce v2.6.1
Nov 23 2015 - Version 2.6
	* Compatibility for The Events Calendar v3.12.6
	* Compatibility for WooCommerce v2.4.10
	* Fixed position of product details hover
	* Added support for the Give donation plugin
	* Updated .pot translation file

June 11 2015 - Version 2.5

	* Updated for Events Calendar 3.9.3 compatibility
	* Updated wow.js animation to version 1.1.2
	* Updated .pot translation file
	* Updated script handles to avoid duplicate loading
	* Updated to Customizer Library version 1.3.0
	* Fixed customizer settings saving
	* Fixed linking in footer copyright customizer textarea
	* Improved data sanitization


May 08 2015 - Version 2.4

	* Updated text domain to 'heartfelt'
	* Updated to Font Awesome 4.3.0
	* Updated functions with theme-slug prefix
	* Added support for 'title-tag'
	* Improved output sanitization
	* Replaced 'do_shortcode' content output with wp_query
	* Improved contact page Google Map customizer options
	* Improved customizer settings to load asynchronously
	* Adding SCSS folder

Apr 29 2015 - Version 2.3

	* Updated TGM plugin activation to version 2.4.2
	* Updated Rescue Shortcodes download link from Github to .org directory


Mar 26 2015 - Version 2.2.1

	* Updated customizer settings to default sections as off
	* Added "Widget Importer & Exporter" to recommended plugin list
	* Added individual demo content xml files

Mar 23 2015 - Version 2.2

	* Updated demo content
	* Updated child theme

Mar 10 2015 - Version 2.1.2

	* Updated license terms

Feb 20 2015 - Version 2.1.1

	* Added site description text in header

Jan 17 2015 - Version 2.1

	* Updated for WooCommerce 2.3
	* Added widget demo .wie file
	* Updated documentation for widget demo content

Dec 17 2014 - Version 2.0

	NOTICE: This version is a major overhaul of the theme. Changes include:

	* Converted to Sass development environment
	* Updated to Foundation 5.4.7
	* Updated translation file
	* Updated to wow.js 1.0.2
	* Updated translation .po/.mo file
	* Updated demo content xml files
	* Updated to Slider Revolution 4.6.5
	* Added additional data validation
	* Added additional customizer options
	* Added additional widgetized footer columns
	* Added individual demo content xml files
	* Removed Content Composer plugin

Dec 1 2014 - Version 1.4

	* Fixed home page blog meta display on Safari

Nov 25 2014 - Version 1.3

	* Added closing 'X' to social sharing modal
	* Fixed date display on posts
	* Fixed navigation and donation alignment

Nov 10 2014 - Version 1.2

	* Added closing 'X' to donation modal
	* Fixed mobile navigation display
	* Fixed mobile logo display
	* Minor style adustments

Sep 26 2014 - Version 1.1

	* Updated to Foundation 5.4.5
	* Recommended Rescue Themes plugins download from Github
	* Updated to Visual Composer 4.3.4
	* Updated to Slider Revolution 4.6.0
	* Updated copyright text in theme options
	* Compatibility check for Woocommerce 2.2.4
	* Added child theme template to main download package
	* Mobile display improvements

Aug 28 2014 - Version 1.0

	* First Release
