msgid ""
msgstr ""
"Project-Id-Version: Vulcan theme\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2012-10-30 13:07+0700\n"
"PO-Revision-Date: 2012-10-30 13:08+0700\n"
"Last-Translator: imediapixel <imediapixel@gmail.com>\n"
"Language-Team: Indonez <templates@indonez.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e\n"
"X-Poedit-Basepath: .\n"
"X-Generator: Poedit 1.5.4\n"
"X-Poedit-SearchPath-0: ..\n"

#: ../404.php:23
msgid "Apologies, but the page you requested could not be found"
msgstr ""

#: ../archive.php:12
msgid "Posts Tagged "
msgstr ""

#: ../archive.php:14 ../archive.php:16 ../archive.php:18
msgid "Archive for "
msgstr ""

#: ../archive.php:20
msgid "Author Archive"
msgstr ""

#: ../archive.php:22
msgid "Blog Archives"
msgstr ""

#: ../archive.php:52 ../blog-template.php:57 ../search.php:45
msgid "posted by"
msgstr ""

#: ../archive.php:52 ../blog-template.php:57 ../search.php:45
msgid "category "
msgstr ""

#: ../archive.php:52 ../blog-template.php:57 ../search.php:45 ../single.php:44
msgid "comments "
msgstr ""

#: ../archive.php:52 ../blog-template.php:57 ../search.php:45 ../single.php:44
msgid "0 Comment"
msgstr ""

#: ../archive.php:52 ../blog-template.php:57 ../search.php:45 ../single.php:44
msgid "1 Comment"
msgstr ""

#: ../archive.php:52 ../blog-template.php:57 ../search.php:45 ../single.php:44
msgid "% Comments"
msgstr ""

#: ../blog-template.php:41 ../search.php:29 ../slideshow.php:15
msgid "Read More"
msgstr ""

#: ../comments.php:6
msgid "Please do not load this page directly. Thanks!"
msgstr ""

#: ../comments.php:9
msgid "This post is password protected. Enter the password to view comments."
msgstr ""

#: ../comments.php:21
msgid "Comment"
msgstr ""

#: ../comments.php:31
msgid "Trackbacks for this post"
msgstr ""

#: ../comments.php:44
msgid "Comments are now closed for this article."
msgstr ""

#: ../comments.php:54
msgid "Comments are closed."
msgstr ""

#: ../comments.php:71
msgid "Name"
msgstr ""

#: ../comments.php:75
msgid "Email"
msgstr ""

#: ../comments.php:79
msgid "Website"
msgstr ""

#: ../comments.php:85
#, php-format
msgid ""
"Logged in as <a href=\"%s\">%s</a>. <a title=\"Log out of this account\" "
"href=\"%s\">Log out?</a></p>\n"
"                    "
msgstr ""

#: ../comments.php:91
msgid "Leave a Reply"
msgstr ""

#: ../comments.php:92
#, php-format
msgid "Leave a Reply to %s"
msgstr ""

#: ../comments.php:93
msgid "Cancel reply"
msgstr ""

#: ../comments.php:94
msgid "Submit"
msgstr ""

#: ../contact-template.php:54
msgid "Name "
msgstr ""

#: ../contact-template.php:55
msgid "Subject "
msgstr ""

#: ../contact-template.php:56
msgid "E-mail "
msgstr ""

#: ../contact-template.php:57
msgid "Message "
msgstr ""

#: ../contact-template.php:61
msgid "Please wait.."
msgstr ""

#: ../footer.php:17
msgid "Phone "
msgstr ""

#: ../footer.php:18
msgid "Fax "
msgstr ""

#: ../footer.php:19
msgid "Email "
msgstr ""

#: ../footer.php:31
msgid "Company News"
msgstr ""

#: ../header.php:109
msgid "Get in Touch Now"
msgstr ""

#: ../portfolio-template.php:42 ../taxonomy-portfolio_category.php:36
msgid "Switch Thumb"
msgstr ""

#: ../portfolio-template.php:52 ../taxonomy-portfolio_category.php:46
msgid "Filter by :"
msgstr ""

#: ../portfolio-template.php:90 ../taxonomy-portfolio_category.php:86
#: ../functions/theme-widgets.php:369
msgid "Read more"
msgstr ""

#: ../portfolio-template.php:95 ../taxonomy-portfolio_category.php:82
msgid "Visit Site"
msgstr ""

#: ../search.php:8
msgid "Search Results for "
msgstr ""

#: ../search.php:63
msgid "No Result found for"
msgstr ""

#: ../search.php:64
msgid "Try different search?"
msgstr ""

#: ../services-template.php:62
msgid "Learn More"
msgstr ""

#: ../sidebar.php:12
msgid "More "
msgstr ""

#: ../single-portfolio.php:15 ../functions/post-types.php:6
msgid "Portfolio"
msgstr ""

#: ../single.php:44
msgid "posted by "
msgstr ""

#: ../admin/admin-functions.php:83
msgid "Elements"
msgstr ""

#: ../admin/admin-functions.php:84
msgid "List Style"
msgstr ""

#: ../admin/admin-functions.php:85
msgid "Dropcap"
msgstr ""

#: ../admin/admin-functions.php:86
msgid "Pullquote Left"
msgstr ""

#: ../admin/admin-functions.php:87
msgid "Pullquote Right"
msgstr ""

#: ../admin/admin-functions.php:88
msgid "Tabs"
msgstr ""

#: ../admin/admin-functions.php:89
msgid "Toggle"
msgstr ""

#: ../admin/admin-functions.php:90
msgid "Image"
msgstr ""

#: ../admin/admin-functions.php:91
msgid "Google Map"
msgstr ""

#: ../admin/admin-functions.php:92
msgid "Youtube"
msgstr ""

#: ../admin/admin-functions.php:93
msgid "Vimeo"
msgstr ""

#: ../admin/admin-functions.php:94
msgid "Button"
msgstr ""

#: ../admin/admin-functions.php:95
msgid "Bullet List"
msgstr ""

#: ../admin/admin-functions.php:96
msgid "Star List"
msgstr ""

#: ../admin/admin-functions.php:97
msgid "Arrow List"
msgstr ""

#: ../admin/admin-functions.php:98
msgid "Square List"
msgstr ""

#: ../admin/admin-functions.php:99
msgid "Check List"
msgstr ""

#: ../admin/admin-functions.php:100
msgid "Delete List"
msgstr ""

#: ../admin/admin-functions.php:101
msgid "Pen List"
msgstr ""

#: ../admin/admin-functions.php:102
msgid "Green Arrow List"
msgstr ""

#: ../admin/admin-functions.php:103
msgid "Gear List"
msgstr ""

#: ../admin/admin-functions.php:104
msgid "Info Box"
msgstr ""

#: ../admin/admin-functions.php:105
msgid "Success Box"
msgstr ""

#: ../admin/admin-functions.php:106
msgid "Warning Box"
msgstr ""

#: ../admin/admin-functions.php:107
msgid "Error Box"
msgstr ""

#: ../admin/admin-functions.php:108
msgid "One Fourth"
msgstr ""

#: ../admin/admin-functions.php:109
msgid "One Fourth Last"
msgstr ""

#: ../admin/admin-functions.php:110
msgid "One Third"
msgstr ""

#: ../admin/admin-functions.php:111
msgid "One Third Last"
msgstr ""

#: ../admin/admin-functions.php:112
msgid "One Half"
msgstr ""

#: ../admin/admin-functions.php:113
msgid "One Half Last"
msgstr ""

#: ../admin/admin-functions.php:114
msgid "Two Third"
msgstr ""

#: ../admin/admin-functions.php:115
msgid "Three Fourth"
msgstr ""

#: ../admin/admin-functions.php:116
msgid "One Fifth"
msgstr ""

#: ../admin/admin-functions.php:117
msgid "One Fifth Last"
msgstr ""

#: ../admin/admin-functions.php:118
msgid "Two Third Last"
msgstr ""

#: ../admin/admin-functions.php:119
msgid "Three Fourth Last "
msgstr ""

#: ../admin/admin-functions.php:120
msgid "Columns"
msgstr ""

#: ../admin/admin-functions.php:121
msgid "Content"
msgstr ""

#: ../admin/admin-functions.php:122
msgid "Pricing Table"
msgstr ""

#: ../admin/admin-functions.php:123
msgid "Accordion"
msgstr ""

#: ../admin/admin-functions.php:124
msgid "Staff List"
msgstr ""

#: ../functions/metabox.php:62
msgid "Sidebar Position"
msgstr ""

#: ../functions/metabox.php:63
msgid "Select default page sidebar widget"
msgstr ""

#: ../functions/metabox.php:316
msgid "Post options"
msgstr ""

#: ../functions/metabox.php:317
msgid "Page options"
msgstr ""

#: ../functions/metabox.php:318
msgid "Slideshow options"
msgstr ""

#: ../functions/metabox.php:319
msgid "Portfolio options"
msgstr ""

#: ../functions/metabox.php:320
msgid "Client options"
msgstr ""

#: ../functions/post-types.php:7
msgid "portfolio"
msgstr ""

#: ../functions/post-types.php:8 ../functions/post-types.php:63
#: ../functions/post-types.php:99 ../functions/post-types.php:139
#: ../functions/post-types.php:179
msgid "Add New"
msgstr ""

#: ../functions/post-types.php:9
msgid "Add New portfolio"
msgstr ""

#: ../functions/post-types.php:10
msgid "Edit portfolio"
msgstr ""

#: ../functions/post-types.php:11
msgid "New portfolio"
msgstr ""

#: ../functions/post-types.php:12
msgid "View portfolio"
msgstr ""

#: ../functions/post-types.php:13
msgid "Search portfolio"
msgstr ""

#: ../functions/post-types.php:14
msgid "No portfolio found"
msgstr ""

#: ../functions/post-types.php:15
msgid "No portfolio found in Trash"
msgstr ""

#: ../functions/post-types.php:45 ../functions/post-types.php:46
msgid "Portfolio Categories"
msgstr ""

#: ../functions/post-types.php:61
msgid "Slideshow"
msgstr ""

#: ../functions/post-types.php:62
msgid "slideshow"
msgstr ""

#: ../functions/post-types.php:64
msgid "Add New slideshow"
msgstr ""

#: ../functions/post-types.php:65
msgid "Edit slideshow"
msgstr ""

#: ../functions/post-types.php:66
msgid "New slideshow"
msgstr ""

#: ../functions/post-types.php:67
msgid "View slideshow"
msgstr ""

#: ../functions/post-types.php:68
msgid "Search slideshow"
msgstr ""

#: ../functions/post-types.php:69
msgid "No slideshow found"
msgstr ""

#: ../functions/post-types.php:70
msgid "No slideshow found in Trash"
msgstr ""

#: ../functions/post-types.php:97 ../functions/post-types.php:98
msgid "Client"
msgstr ""

#: ../functions/post-types.php:100
msgid "Add New Client"
msgstr ""

#: ../functions/post-types.php:101
msgid "Edit Client"
msgstr ""

#: ../functions/post-types.php:102
msgid "New Client"
msgstr ""

#: ../functions/post-types.php:103
msgid "View Client"
msgstr ""

#: ../functions/post-types.php:104
msgid "Search Client"
msgstr ""

#: ../functions/post-types.php:105
msgid "No Client found"
msgstr ""

#: ../functions/post-types.php:106
msgid "No Client found in Trash"
msgstr ""

#: ../functions/post-types.php:137 ../functions/post-types.php:138
msgid "Testimonial"
msgstr ""

#: ../functions/post-types.php:140
msgid "Add New Testimonial"
msgstr ""

#: ../functions/post-types.php:141
msgid "Edit Testimonial"
msgstr ""

#: ../functions/post-types.php:142
msgid "New Testimonial"
msgstr ""

#: ../functions/post-types.php:143
msgid "View Testimonial"
msgstr ""

#: ../functions/post-types.php:144
msgid "Search Testimonial"
msgstr ""

#: ../functions/post-types.php:145
msgid "No Testimonial found"
msgstr ""

#: ../functions/post-types.php:146
msgid "No Testimonial found in Trash"
msgstr ""

#: ../functions/post-types.php:177 ../functions/post-types.php:178
msgid "Staff"
msgstr ""

#: ../functions/post-types.php:180
msgid "Add New Staff"
msgstr ""

#: ../functions/post-types.php:181
msgid "Edit Staff"
msgstr ""

#: ../functions/post-types.php:182
msgid "New Staff"
msgstr ""

#: ../functions/post-types.php:183
msgid "View Staff"
msgstr ""

#: ../functions/post-types.php:184
msgid "Search Staff"
msgstr ""

#: ../functions/post-types.php:185
msgid "No Staff found"
msgstr ""

#: ../functions/post-types.php:186
msgid "No Staff found in Trash"
msgstr ""

#: ../functions/theme-functions.php:31
#, php-format
msgid "%1$s at %2$s"
msgstr ""

#: ../functions/theme-functions.php:31
msgid "(Edit)"
msgstr ""

#: ../functions/theme-functions.php:35
msgid "Your comment is awaiting moderation."
msgstr ""

#: ../functions/theme-functions.php:84
msgid "Main Navigation"
msgstr ""

#: ../functions/theme-functions.php:85
msgid "Footer Navigation"
msgstr ""

#: ../functions/theme-functions.php:140 ../functions/theme-functions.php:152
msgid "Unable to parse URL"
msgstr ""

#: ../functions/theme-functions.php:257
msgid "Thumbnail"
msgstr ""

#: ../functions/theme-functions.php:282
msgid "None"
msgstr ""

#: ../functions/theme-widgets.php:236
msgid "Testimonials"
msgstr ""

#: ../functions/theme-widgets.php:412
msgid "Our Clients"
msgstr ""

#: ../functions/inc/template-tags.php:205
msgid "Missing Attachment"
msgstr ""
