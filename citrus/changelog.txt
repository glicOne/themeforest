*** Citrus - One Page Parallax Portfolio Theme Change log ***


2016.08.23 - version 1.9

 * WordPress 4.6 Compatible
 * WPBakery Visual Composer and Ultimate Addons for Visual Composer support
 * Included WPBakery Visual Composer 4.12 and Ultimate Addons for Visual Composer 3.16.5


2016.07.02 - version 1.8
 * WordPress 4.5.3 compatible
 * Responsive Styled Google Maps 3.4 included 
 * LayerSlider WP 5.6.9 included
 * Slider Revolution 5.2.6 included
 * WooCommerce 2.6.2 compatible


2016.04.15 - version 1.7
 * WordPress 4.5 compatible

2015.06.23 - Version 1.6
 * Updated PrettyPhoto Script to latest version 3.1.6

2015.04.24 - Version 1.5
 * XSS vulnerability issues fixed
 * Wordpress 4.2 compatible

2015.04.10 - version 1.4
 * Double post issue in blog section
 * Image not saving issue in quick edit of portfolio backend
 * WooCommerce 2.3.7 compatible
 * Some issues in WPML compatibility


2015.01.02 - version 1.3
 * Image Optimization
 * Image Dimensions are specified
 * Supports both CSS and Jquery minify					


2014.12.25 - version 1.2
 * Retina and RTL support.
 * Can have different menus for different header styles.
 * Updated dummy content file.
 * Added demo slider zip files for both LayerSlider and RevolutionSlider.
 * Option to disable placeholder images.
 * Theme and plugin languages files updated


2014.11.01 - version 1.1
 * Added 6 different header styles,
	1) Slidebar Navigation - Left
	2) Slidebar Navigation - Right
	3) Vertical Navigation - Left
	4) Vertical Navigation - Right
	5) Toggle Header
	6) Menu Over Slider, Image, Special Shortcodes


2014.10.18 - version 1.0
 * First release!