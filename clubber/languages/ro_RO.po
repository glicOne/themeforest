msgid ""
msgstr ""
"Project-Id-Version: Clubber\n"
"POT-Creation-Date: 2014-10-06 21:17+0200\n"
"PO-Revision-Date: 2014-10-06 22:12+0200\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.9\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;"
"_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-SearchPath-0: .\n"

#: 404.php:6
msgid "404 ERROR - Not Found"
msgstr ""

#: 404.php:8
msgid "The page you requested does not exist."
msgstr ""

#: admin/audio.php:15 admin/audio.php:16
msgid "Audio"
msgstr ""

#: admin/audio.php:17 admin/event.php:15 admin/photo.php:13 admin/slide.php:12
#: admin/video.php:13
msgid "Add New"
msgstr ""

#: admin/audio.php:18
msgid "Add New Audio"
msgstr ""

#: admin/audio.php:19 admin/event.php:17 admin/photo.php:15 admin/slide.php:14
#: admin/video.php:15
msgid "Edit"
msgstr ""

#: admin/audio.php:20
msgid "Edit Audio"
msgstr ""

#: admin/audio.php:21
msgid "New Audio"
msgstr ""

#: admin/audio.php:22 admin/audio.php:23
msgid "View Audio"
msgstr ""

#: admin/audio.php:24
msgid "Search Audio"
msgstr ""

#: admin/audio.php:25
msgid "No audio found"
msgstr ""

#: admin/audio.php:26
msgid "No audio found in Trash"
msgstr ""

#: admin/audio.php:54 admin/event.php:52 admin/photo.php:49 admin/video.php:49
msgid "Category"
msgstr ""

#: admin/audio.php:61 admin/event.php:59 admin/photo.php:86 admin/slide.php:44
#: admin/video.php:76
msgid "Title"
msgstr ""

#: admin/audio.php:62 admin/event.php:60 admin/photo.php:87 admin/slide.php:45
#: admin/video.php:77
msgid "Author"
msgstr ""

#: admin/audio.php:63 admin/event.php:61 admin/photo.php:88 admin/slide.php:46
#: admin/video.php:78
msgid "ID"
msgstr ""

#: admin/audio.php:64 admin/event.php:62 admin/photo.php:89 admin/slide.php:47
#: admin/video.php:79
msgid "Date"
msgstr ""

#: admin/audio.php:83 includes/widgets/widget-audio.php:91 single-audio.php:46
msgid "Release date"
msgstr ""

#: admin/audio.php:89
msgid "Event Date (mm/dd/yy)"
msgstr ""

#: admin/audio.php:104 includes/widgets/widget-audio.php:87
#: single-audio.php:42
msgid "Genre"
msgstr ""

#: admin/audio.php:110
msgid "Genre of music"
msgstr ""

#: admin/audio.php:124
msgid "Buy Link"
msgstr ""

#: admin/event.php:13
msgid "Events"
msgstr ""

#: admin/event.php:14
msgid "Event"
msgstr ""

#: admin/event.php:16
msgid "Add New Event"
msgstr ""

#: admin/event.php:18
msgid "Edit Event"
msgstr ""

#: admin/event.php:19
msgid "New Event"
msgstr ""

#: admin/event.php:20 admin/event.php:21
msgid "View Event"
msgstr ""

#: admin/event.php:22
msgid "Search Events"
msgstr ""

#: admin/event.php:23
msgid "No events found"
msgstr ""

#: admin/event.php:24
msgid "No events found in Trash"
msgstr ""

#: admin/event.php:81
msgid "Event settings"
msgstr ""

#: admin/event.php:118
msgid "Event Venue"
msgstr ""

#: admin/event.php:123
msgid "Event Location"
msgstr ""

#: admin/event.php:128
msgid "Event Date (yyyy/mm/dd)"
msgstr ""

#: admin/event.php:133
msgid "Event Time (hh:mm tt)"
msgstr ""

#: admin/event.php:146
msgid "Map"
msgstr ""

#: admin/event.php:166
msgid "Ticket URL"
msgstr ""

#: admin/event.php:171
msgid "Text Button"
msgstr ""

#: admin/photo.php:11 admin/photo.php:107
msgid "Photos"
msgstr ""

#: admin/photo.php:12
msgid "Photo"
msgstr ""

#: admin/photo.php:14
msgid "Add New Photo"
msgstr ""

#: admin/photo.php:16
msgid "Edit Photo"
msgstr ""

#: admin/photo.php:17
msgid "New Photo"
msgstr ""

#: admin/photo.php:18 admin/photo.php:19
msgid "View Photo"
msgstr ""

#: admin/photo.php:20
msgid "Search Photos"
msgstr ""

#: admin/photo.php:21
msgid "No photos found"
msgstr ""

#: admin/photo.php:22
msgid "No photos found in Trash"
msgstr ""

#: admin/photo.php:124
msgid "Upload your photos"
msgstr ""

#: admin/slide.php:10
msgid "Slides"
msgstr ""

#: admin/slide.php:11
msgid "Slide"
msgstr ""

#: admin/slide.php:13
msgid "Add New Slide"
msgstr ""

#: admin/slide.php:15
msgid "Edit Slide"
msgstr ""

#: admin/slide.php:16
msgid "New Slide"
msgstr ""

#: admin/slide.php:17 admin/slide.php:18
msgid "View Slide"
msgstr ""

#: admin/slide.php:19
msgid "Search Slides"
msgstr ""

#: admin/slide.php:20
msgid "No slides found"
msgstr ""

#: admin/slide.php:21
msgid "No slides found in Trash"
msgstr ""

#: admin/slide.php:66
msgid "Slide settings"
msgstr ""

#: admin/slide.php:84
msgid "Slide Description"
msgstr ""

#: admin/slide.php:89
msgid "Slide URL"
msgstr ""

#: admin/theme_options/options-framework.php:261
msgid "Restore Defaults"
msgstr ""

#: admin/theme_options/options-framework.php:261
msgid "Click OK to reset. Any theme settings will be lost!"
msgstr ""

#: admin/theme_options/options-framework.php:264
msgid "Save Options"
msgstr ""

#: admin/theme_options/options-framework.php:297
msgid "Default options restored."
msgstr ""

#: admin/theme_options/options-framework.php:338
msgid "Options saved."
msgstr ""

#: admin/theme_options/options-framework.php:396
msgid "Theme Options"
msgstr ""

#: admin/theme_options/options-medialibrary-uploader.php:127
msgid "Upload"
msgstr ""

#: admin/theme_options/options-medialibrary-uploader.php:150
msgid "View File"
msgstr ""

#: admin/video.php:11
msgid "Videos"
msgstr ""

#: admin/video.php:12
msgid "Video"
msgstr ""

#: admin/video.php:14
msgid "Add New Video"
msgstr ""

#: admin/video.php:16
msgid "Edit Video"
msgstr ""

#: admin/video.php:17
msgid "New Video"
msgstr ""

#: admin/video.php:18 admin/video.php:19
msgid "View Video"
msgstr ""

#: admin/video.php:20
msgid "Search Videos"
msgstr ""

#: admin/video.php:21
msgid "No video found"
msgstr ""

#: admin/video.php:22
msgid "No videos found in Trash"
msgstr ""

#: admin/video.php:55
msgid "YouTube or Vimeo - Video"
msgstr ""

#: archive.php:61 blog-style1.php:86 blog-style2.php:88
#: includes/shortcodes/shortcode-posts.php:56 index.php:58 search.php:69
#: single.php:57
msgid "No comments"
msgstr "nici un comentariu"

#: archive.php:63 blog-style1.php:88 blog-style2.php:90
#: includes/shortcodes/shortcode-posts.php:58 index.php:60 search.php:71
#: single.php:59
msgid "One comment"
msgstr ""

#: archive.php:65 blog-style1.php:90 blog-style2.php:92
#: includes/shortcodes/shortcode-posts.php:60 index.php:62 search.php:73
#: single.php:61
msgid "comments"
msgstr ""

#: archive.php:71 blog-style1.php:96 blog-style2.php:97
#: includes/shortcodes/shortcode-posts.php:65 index.php:69 search.php:79
msgid "Read more"
msgstr ""

#: comments.php:6
msgid ""
"This post is password protected. Enter the password to view any comments."
msgstr ""

#: comments.php:16
#, php-format
msgid "One Comment %2$s"
msgid_plural "%1$s Comments"
msgstr[0] ""
msgstr[1] ""

#: comments.php:24
msgid "<span class=\"meta-nav\">&larr;</span> Older Comments"
msgstr ""

#: comments.php:27
msgid "Newer Comments <span class=\"meta-nav\">&rarr;</span>"
msgstr ""

#: comments.php:50
msgid "Comments are closed."
msgstr ""

#: contact.php:109
msgid "Thanks, your email was sent successfully."
msgstr ""

#: contact.php:121
msgid "Name"
msgstr ""

#: contact.php:130
msgid "&larr; Please enter your name."
msgstr ""

#: contact.php:136
msgid "Email"
msgstr ""

#: contact.php:145
msgid "&larr; Please enter a valid email address."
msgstr ""

#: contact.php:151
msgid "Message"
msgstr ""

#: contact.php:166
msgid "&larr; Please enter a message."
msgstr ""

#: events-style1.php:110 events-style2.php:110 events-style2.php:210
#: events-style3.php:115 events-style3.php:219
#: includes/shortcodes/shortcode-posts.php:156
#: includes/shortcodes/shortcode-posts.php:282
msgid "All Day"
msgstr ""

#: events-style1.php:127 events-style2.php:126 events-style2.php:226
#: events-style3.php:132 events-style3.php:236
#: includes/shortcodes/shortcode-posts.php:172
#: includes/shortcodes/shortcode-posts.php:298
#: includes/widgets/widget-events.php:80 single-event.php:112
msgid "Sold Out"
msgstr ""

#: events-style1.php:130 events-style2.php:129 events-style2.php:229
#: events-style3.php:135 events-style3.php:239
#: includes/shortcodes/shortcode-posts.php:175
#: includes/shortcodes/shortcode-posts.php:301
#: includes/widgets/widget-events.php:83 single-event.php:115
msgid "Canceled"
msgstr ""

#: events-style1.php:133 events-style2.php:132 events-style2.php:232
#: events-style3.php:138 events-style3.php:242
#: includes/shortcodes/shortcode-posts.php:178
#: includes/shortcodes/shortcode-posts.php:304
#: includes/widgets/widget-events.php:86 single-event.php:118
msgid "Free Entry"
msgstr ""

#: events-style1.php:136 events-style2.php:135 events-style2.php:235
#: events-style3.php:141 events-style3.php:245
#: includes/shortcodes/shortcode-posts.php:181
#: includes/shortcodes/shortcode-posts.php:307
#: includes/widgets/widget-events.php:89 single-event.php:121
msgid "Buy Tickets"
msgstr "cumpara bilete"

#: events-style2.php:56 events-style3.php:56
msgid "Upcoming Events"
msgstr ""

#: events-style2.php:147 events-style3.php:153
msgid "Sorry, no events coming up."
msgstr ""

#: events-style2.php:153 events-style3.php:159
msgid "Past Events"
msgstr ""

#: events-style2.php:247 events-style3.php:256
msgid "Sorry, no events past."
msgstr ""

#: footer.php:61
msgid "All Rights Reserved."
msgstr ""

#: header.php:23
msgid "Archive"
msgstr ""

#: header.php:28
msgid "Search for"
msgstr ""

#: header.php:35
msgid "Not Found"
msgstr ""

#: includes/functions-comment.php:28
#, php-format
msgid "%s <span class=\"says\">says:</span>"
msgstr ""

#: includes/functions-comment.php:35
msgid "Your comment is awaiting moderation."
msgstr ""

#. translators: 1: date, 2: time
#: includes/functions-comment.php:47
#, php-format
msgid "%1$s at %2$s"
msgstr ""

#: includes/functions-comment.php:49 includes/functions-comment.php:79
msgid "(Edit)"
msgstr ""

#: includes/functions-comment.php:75
msgid "Pingback:"
msgstr ""

#: includes/functions-layout.php:9
msgid "Sidebar layout settings"
msgstr ""

#: includes/functions-menu.php:4
msgid "Top Menu"
msgstr ""

#: includes/functions-sidebar.php:7
msgid "Sidebar Page"
msgstr ""

#: includes/functions-sidebar.php:8
msgid "Page sidebar"
msgstr ""

#: includes/functions-sidebar.php:22
msgid "Footer Widget 1"
msgstr ""

#: includes/functions-sidebar.php:23 includes/functions-sidebar.php:59
msgid "In the footer the left column"
msgstr ""

#: includes/functions-sidebar.php:34
msgid "Footer Widget 2"
msgstr ""

#: includes/functions-sidebar.php:35
msgid "In the footer the center left column"
msgstr ""

#: includes/functions-sidebar.php:46
msgid "Footer Widget 3"
msgstr ""

#: includes/functions-sidebar.php:47
msgid "In the footer the center right column"
msgstr ""

#: includes/functions-sidebar.php:58
msgid "Footer Widget 4"
msgstr ""

#: includes/shortcodes/shortcode-soundcloud.php:193
msgid "You do not have sufficient permissions to access this page."
msgstr ""

#: includes/shortcodes/shortcode-soundcloud.php:333
msgid "Save Changes"
msgstr ""

#: includes/widgets/widget-audio.php:10
msgid "Display your Audio Player."
msgstr ""

#: includes/widgets/widget-audio.php:12
msgid "CLUBBER - Audio Player"
msgstr ""

#: includes/widgets/widget-audio.php:204 includes/widgets/widget-blog.php:93
#: includes/widgets/widget-events.php:130
#: includes/widgets/widget-flickr.php:77
#: includes/widgets/widget-photos.php:103
#: includes/widgets/widget-soundcloud.php:71
#: includes/widgets/widget-twitter.php:116
#: includes/widgets/widget-videos.php:96
msgid "Title:"
msgstr ""

#: includes/widgets/widget-audio.php:219
#: includes/widgets/widget-photos.php:118
msgid "Post ID:"
msgstr ""

#: includes/widgets/widget-audio.php:232
msgid "(optional) SoundCloud API:"
msgstr ""

#: includes/widgets/widget-blog.php:10
msgid "Recent posts on blog your site."
msgstr ""

#: includes/widgets/widget-blog.php:12
msgid "CLUBBER - Recent Posts"
msgstr ""

#: includes/widgets/widget-blog.php:108 includes/widgets/widget-events.php:145
#: includes/widgets/widget-videos.php:111
msgid "Posts Number:"
msgstr ""

#: includes/widgets/widget-events.php:10
msgid "Display your Upcoming Events."
msgstr ""

#: includes/widgets/widget-events.php:12
msgid "CLUBBER - Upcoming Events"
msgstr ""

#: includes/widgets/widget-flickr.php:10
msgid "Display images from a Flickr account."
msgstr ""

#: includes/widgets/widget-flickr.php:12
msgid "CLUBBER - Flickr"
msgstr ""

#: includes/widgets/widget-photos.php:10
msgid "Display your Photos."
msgstr ""

#: includes/widgets/widget-photos.php:12
msgid "CLUBBER - Photos"
msgstr ""

#: includes/widgets/widget-photos.php:133
msgid "Number of Photos:"
msgstr ""

#: includes/widgets/widget-soundcloud.php:10
msgid "Display your SoundCloud."
msgstr ""

#: includes/widgets/widget-soundcloud.php:12
msgid "CLUBBER - SoundCloud"
msgstr ""

#: includes/widgets/widget-soundcloud.php:88
msgid "SoundCloud API:"
msgstr ""

#: includes/widgets/widget-twitter.php:11
msgid "Display your latest Tweets"
msgstr ""

#: includes/widgets/widget-twitter.php:13
msgid "CLUBBER - Twitter"
msgstr ""

#: includes/widgets/widget-twitter.php:131
msgid "Consumer key"
msgstr ""

#: includes/widgets/widget-twitter.php:146
msgid "Consumer secret"
msgstr ""

#: includes/widgets/widget-twitter.php:161
msgid "Access token"
msgstr ""

#: includes/widgets/widget-twitter.php:176
msgid "Access token secret"
msgstr ""

#: includes/widgets/widget-twitter.php:191
msgid "Username"
msgstr ""

#: includes/widgets/widget-twitter.php:206
msgid "Number of tweets"
msgstr ""

#: includes/widgets/widget-videos.php:10
msgid "Display your latest Videos."
msgstr ""

#: includes/widgets/widget-videos.php:12
msgid "CLUBBER - Latest Videos"
msgstr ""

#: index.php:30
msgid "Recent Posts"
msgstr ""

#: search.php:8
msgid "Search Results for:"
msgstr ""

#: search.php:84
msgid "No posts were found!"
msgstr ""

#: searchform.php:5
msgid "Search here ..."
msgstr ""

#: searchform.php:6
msgid "GO"
msgstr ""

#: single-event.php:90
msgid "Location"
msgstr ""

#: single-event.php:94
msgid "Venue"
msgstr ""

#: single-event.php:97 single-event.php:99
msgid "Length"
msgstr ""
