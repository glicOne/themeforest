msgid ""
msgstr ""
"Project-Id-Version: Bookcase Portfolio Theme\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2011-10-12 18:51-0600\n"
"PO-Revision-Date: 2011-10-12 18:51-0600\n"
"Last-Translator: Andre Gagnon <support@designcirc.us>\n"
"Language-Team: 2winFactor <support@designcirc.us>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-KeywordsList: __;_e\n"
"X-Poedit-Basepath: ./\n"
"X-Poedit-Language: English\n"
"X-Poedit-Country: UNITED STATES\n"
"X-Poedit-SearchPath-0: ..\n"

#: ../404.php:7
msgid "Page Not Found"
msgstr ""

#: ../404.php:7
#: ../404.php:20
#: ../index.php:66
#: ../page.php:27
#: ../search.php:59
#: ../single-portfolio.php:151
#: ../single.php:91
#: ../template-full.php:28
msgid "Sorry, but you are looking for something that isn't here."
msgstr ""

#: ../404.php:19
#: ../index.php:65
#: ../page.php:26
#: ../search.php:58
#: ../single-portfolio.php:150
#: ../single.php:90
#: ../template-full.php:27
msgid "Error 404 - Not found."
msgstr ""

#: ../comments.php:8
#: ../comments.php:52
msgid "This post is password protected. Enter the password to view comments."
msgstr ""

#: ../comments.php:16
msgid "Comments"
msgstr ""

#: ../comments.php:47
msgid "Submit a Comment"
msgstr ""

#: ../comments.php:57
msgid "You must be logged in to post a comment."
msgstr ""

#: ../comments.php:57
msgid "Login"
msgstr ""

#: ../comments.php:61
msgid "Logged in as"
msgstr ""

#: ../comments.php:61
msgid "Logout."
msgstr ""

#: ../comments.php:64
#: ../template-contact.php:231
#: ../template-contact.php:287
#: ../template-contact.php:331
msgid "Name"
msgstr ""

#: ../comments.php:70
msgid "Mail"
msgstr ""

#: ../comments.php:76
#: ../template-contact.php:361
msgid "Website"
msgstr ""

#: ../comments.php:81
#: ../template-contact.php:250
#: ../template-contact.php:306
#: ../template-contact.php:419
msgid "Message"
msgstr ""

#: ../functions.php:385
msgid "Y/m/d"
msgstr ""

#: ../functions.php:387
msgid "Re-Attach"
msgstr ""

#: ../functions.php:392
msgid "(Unattached)"
msgstr ""

#: ../functions.php:393
msgid "Attach"
msgstr ""

#: ../functions.php:434
msgid "Your comment is awaiting moderation."
msgstr ""

#: ../functions.php:496
msgid "Portfolio"
msgstr ""

#: ../functions.php:497
msgid "Portfolio Item"
msgstr ""

#: ../functions.php:498
msgid "Add New"
msgstr ""

#: ../functions.php:499
msgid "Add New Portfolio Item"
msgstr ""

#: ../functions.php:500
msgid "Edit"
msgstr ""

#: ../functions.php:501
msgid "Edit Portfolio Item"
msgstr ""

#: ../functions.php:502
msgid "New Portfolio Item"
msgstr ""

#: ../functions.php:503
msgid "View Portfolio"
msgstr ""

#: ../functions.php:504
msgid "View Portfolio Item"
msgstr ""

#: ../functions.php:505
msgid "Search Portfolio Items"
msgstr ""

#: ../functions.php:506
msgid "No Portfolios found"
msgstr ""

#: ../functions.php:507
msgid "No Portfolio Items found in Trash"
msgstr ""

#: ../functions.php:508
msgid "Parent Portfolio"
msgstr ""

#: ../functions.php:532
msgid "Search Skills"
msgstr ""

#: ../functions.php:533
msgid "All Skills"
msgstr ""

#: ../functions.php:534
msgid "Parent Skill"
msgstr ""

#: ../functions.php:535
msgid "Parent Skill:"
msgstr ""

#: ../functions.php:536
msgid "Edit Skill"
msgstr ""

#: ../functions.php:537
msgid "Update Skill"
msgstr ""

#: ../functions.php:538
msgid "Add New Skill"
msgstr ""

#: ../functions.php:539
msgid "New Skill Name"
msgstr ""

#: ../functions.php:540
msgid "Skills"
msgstr ""

#: ../header.php:32
#, php-format
msgid "Page %s"
msgstr ""

#: ../header.php:94
msgid "To Top"
msgstr ""

#: ../index.php:23
#: ../index.php:30
#: ../search.php:19
#: ../search.php:26
#: ../template-home.php:123
#, php-format
msgid "Permanent Link to %s"
msgstr ""

#: ../index.php:37
#: ../search.php:33
#: ../single-portfolio.php:94
#: ../single.php:37
#: ../template-home.php:127
msgid "Read more..."
msgstr ""

#: ../index.php:38
#: ../search.php:34
#: ../single-portfolio.php:95
#: ../single.php:38
msgid "Edit Post"
msgstr ""

#: ../index.php:53
#: ../search.php:48
#: ../single-portfolio.php:120
#: ../single.php:63
#: ../template-home.php:118
msgid "No Comments"
msgstr ""

#: ../index.php:53
#: ../search.php:48
#: ../template-home.php:118
msgid "One Comment"
msgstr ""

#: ../index.php:53
#: ../search.php:48
#: ../single-portfolio.php:120
#: ../single.php:63
#: ../template-home.php:118
msgid "% Comments"
msgstr ""

#: ../index.php:54
msgid "Posted in"
msgstr ""

#: ../index.php:55
msgid "Post by"
msgstr ""

#: ../index.php:107
msgid "&larr; Older"
msgstr ""

#: ../index.php:108
msgid "Newer &rarr;"
msgstr ""

#: ../search.php:9
msgid "Search Results for:"
msgstr ""

#: ../search.php:48
msgid "Posted In"
msgstr ""

#: ../search.php:48
msgid "Posted By"
msgstr ""

#: ../search.php:79
#: ../search.php:81
#: ../single-portfolio.php:159
#: ../single-portfolio.php:161
#: ../single.php:99
#: ../single.php:101
msgid "Previous"
msgstr ""

#: ../search.php:97
#: ../search.php:99
#: ../single-portfolio.php:177
#: ../single-portfolio.php:179
#: ../single.php:117
#: ../single.php:119
msgid "Next"
msgstr ""

#: ../searchform.php:4
msgid "To search type and hit enter"
msgstr ""

#: ../single-portfolio.php:64
#: ../single-portfolio.php:68
#: ../single-portfolio.php:72
#: ../single-portfolio.php:76
#: ../single-portfolio.php:80
msgid "Open in Lightbox"
msgstr ""

#: ../single-portfolio.php:109
#: ../single.php:52
msgid "Posted By:"
msgstr ""

#: ../single-portfolio.php:117
#: ../single.php:60
msgid "Comments:"
msgstr ""

#: ../single-portfolio.php:120
#: ../single.php:63
msgid "1 Comment"
msgstr ""

#: ../single-portfolio.php:125
msgid "Skills:"
msgstr ""

#: ../single.php:68
msgid "Posted in:"
msgstr ""

#: ../template-contact.php:42
#: ../template-contact.php:108
msgid "You forgot to fill in your name"
msgstr ""

#: ../template-contact.php:52
#: ../template-contact.php:118
msgid "Your forgot to fill in your email address"
msgstr ""

#: ../template-contact.php:56
#: ../template-contact.php:122
msgid "Wrong email format"
msgstr ""

#: ../template-contact.php:67
msgid "You forgot to fill in your message"
msgstr ""

#: ../template-contact.php:81
msgid "Contact from your site"
msgstr ""

#: ../template-contact.php:133
msgid "You forgot to fill in your phone number."
msgstr ""

#: ../template-contact.php:143
msgid "You forgot to select a service."
msgstr ""

#: ../template-contact.php:153
msgid "You forgot to select a budget."
msgstr ""

#: ../template-contact.php:175
msgid "Quote Request From Your Site"
msgstr ""

#: ../template-contact.php:240
#: ../template-contact.php:296
#: ../template-contact.php:340
msgid "Email"
msgstr ""

#: ../template-contact.php:259
#: ../template-contact.php:315
msgid "Send Message"
msgstr ""

#: ../template-contact.php:352
msgid "Phone"
msgstr ""

#: ../template-contact.php:369
msgid "Project Name"
msgstr ""

#: ../template-contact.php:375
msgid "Required Service"
msgstr ""

#: ../template-contact.php:399
msgid "Project Budget"
msgstr ""

#: ../template-contact.php:425
msgid "Request Quote"
msgstr ""

#: ../template-home.php:119
msgid "No Tags"
msgstr ""

#: ../template-home.php:120
msgid "Added on"
msgstr ""

#: ../admin/theme-functions.php:123
#, php-format
msgid "View all projects filed under %s"
msgstr ""

#: ../admin/theme-functions.php:139
#, php-format
msgid "Feed for all posts filed under %s"
msgstr ""

#: ../admin/theme-options.php:112
msgid "Theme Skin"
msgstr ""

#: ../functions/customfields.php:20
msgid "Slide 1 Image"
msgstr ""

#: ../functions/customfields.php:28
msgid "Select a Slide 1 Image"
msgstr ""

#: ../functions/customfields.php:34
#: ../functions/customfields.php:42
msgid "Select a Slide 2 Image"
msgstr ""

#: ../functions/customfields.php:48
#: ../functions/customfields.php:56
msgid "Select a Slide 3 Image"
msgstr ""

#: ../functions/customfields.php:62
#: ../functions/customfields.php:70
msgid "Select a Slide 4 Image"
msgstr ""

#: ../functions/customfields.php:76
#: ../functions/customfields.php:84
#: ../functions/customfields.php:98
msgid "Select a Slide 5 Image"
msgstr ""

#: ../functions/customfields.php:90
msgid "Select a Slide 6 Image"
msgstr ""

#: ../functions/customfields.php:108
msgid "Video Settings"
msgstr ""

#: ../functions/customfields.php:114
msgid "Youtube or Vimeo URL"
msgstr ""

#: ../functions/customfields.php:115
msgid "If you are using youtube or Vimeo, please enter in the URL here."
msgstr ""

#: ../functions/customfields.php:188
msgid "These settings enable you to provide videos for your viewers to watch."
msgstr ""

#: ../functions/custompagefields.php:87
msgid "Optional Page Content"
msgstr ""

#: ../functions/update-notice.php:61
msgid "An Unexpected HTTP Error occurred during the API request.</p> <p><a href=\"?\" onclick=\"document.location.reload(); return false;\">Try again</a>"
msgstr ""

#: ../functions/update-notice.php:66
msgid "An unknown error occurred"
msgstr ""

#: ../functions/widget-contact.php:49
#: ../functions/widget-twitter.php:29
msgid "A widget that displays your latest tweets."
msgstr ""

#: ../functions/widget-contact.php:61
msgid "Custom Quick Contact Widget"
msgstr ""

#: ../functions/widget-contact.php:203
msgid "Contact Title (Optional):"
msgstr ""

#: ../functions/widget-contact.php:209
msgid "Email:"
msgstr ""

#: ../functions/widget-contact.php:215
msgid "Contact Page URL:"
msgstr ""

#: ../functions/widget-contact.php:221
#: ../functions/widget-twitter.php:133
msgid "Button Text:"
msgstr ""

#: ../functions/widget-divider.php:29
msgid "A widget that displays a divider with optional titling."
msgstr ""

#: ../functions/widget-divider.php:35
msgid "-----Custom Divider Widget-----"
msgstr ""

#: ../functions/widget-divider.php:98
msgid "Heading Above Divider (Optional)"
msgstr ""

#: ../functions/widget-divider.php:104
msgid "Divider Titling (Optional):"
msgstr ""

#: ../functions/widget-news.php:29
msgid "A widget that displays the latest post(s) from a category."
msgstr ""

#: ../functions/widget-news.php:35
msgid "Custom News Widget"
msgstr ""

#: ../functions/widget-news.php:114
msgid "News Title (Optional):"
msgstr ""

#: ../functions/widget-news.php:120
#: ../functions/widget-recent-projects.php:112
msgid "Number of Posts:"
msgstr ""

#: ../functions/widget-news.php:127
msgid "Choose a Category:"
msgstr ""

#: ../functions/widget-project.php:29
msgid "A widget that displays a project thumbnail and lightbox."
msgstr ""

#: ../functions/widget-project.php:35
msgid "Custom Project Image Widget"
msgstr ""

#: ../functions/widget-project.php:111
msgid "Project Image or Video (Any Size):"
msgstr ""

#: ../functions/widget-project.php:117
msgid "Project Thumbnail URL (200px x 155px):"
msgstr ""

#: ../functions/widget-project.php:124
msgid "Project Title:"
msgstr ""

#: ../functions/widget-project.php:130
msgid "Description (Optional):"
msgstr ""

#: ../functions/widget-recent-projects.php:29
msgid "A widget that displays small project thumbnails from recent projects."
msgstr ""

#: ../functions/widget-recent-projects.php:35
msgid "Custom Recent Projects Widget"
msgstr ""

#: ../functions/widget-recent-projects.php:106
msgid "Projects Title (Optional):"
msgstr ""

#: ../functions/widget-twitter.php:35
msgid "Custom Twitter Widget"
msgstr ""

#: ../functions/widget-twitter.php:115
msgid "Twitter Title (Optional):"
msgstr ""

#: ../functions/widget-twitter.php:121
msgid "Twitter Username:"
msgstr ""

#: ../functions/widget-twitter.php:127
msgid "Number of Tweets:"
msgstr ""

