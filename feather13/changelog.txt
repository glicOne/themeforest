  Theme : Feather
Version : 1.3
 Author : WPBandit - http://wpbandit.com

.: January 24 2013 (v1.3) :.
===========================================================
- Added portfolio archive, category pages and pagination
- Fixed slow portfolio preload
- Moved all page templates to /page-templates/ folder
- Added option to disable responsive layout
- Added minimal form base css
- Removed outdated and unused form framework css
- Removed custom comments-form, now uses default form for better plugin compatibility
- Changed sticky footer to load on window.load to fix issue on certain pages
- Moved respond.min.js to load in footer to work properly in IE8 and older
- Minor CSS bug fixes
- Updated .pot language file

.: October 22 2012 (v1.2) :.
===========================================================
- Updated to Air Framework 1.2
- Updated documentation
- Added "Frontpage Boxed" template
- Added "Frontpage Wide" template
- Added shortcode dropdown selection in editor
- Added unlimited sidebars functionality
- Added support for single post templates
- Added new icons to social module
- Added support for HTML5 videos in embed field
- Improved maintenance module
- Fixed default page title in SEO options
- Fixed dropdowns being hidden behind youtube embeds in IE
- Added fancybox integration with default WP galleries
- Improved portfolio thumbnails. Lightbox now loads first image on single page instead
- Added content area in portfolio page template
- Added styling option to limit vertical images height in galleries
- Added pre-load to portfolio for a smoother viewing experience
- Fixed sticky footer in portfolio
- Fixed missing global background image for search, archive and 404
- Minor bug fixes

.: August 24 2012 (v1.1) :.
===========================================================
- Added option to stick header to top when scrolling
- Added option to disable glass effect
- Fixed issue with custom-functions.php
- Framework updates

.: August 23 2012 (v1.0) :.
===========================================================
- Theme released