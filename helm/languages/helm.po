msgid ""
msgstr ""
"Project-Id-Version: Helm\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-05-19 21:52+0530\n"
"PO-Revision-Date: 2013-05-19 21:52+0530\n"
"Last-Translator: imaginem <imagine.themes@gmail.com>\n"
"Language-Team: imaginem <imagine.themes@gmail.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-KeywordsList: __;_e\n"
"X-Poedit-Basepath: .\n"
"X-Generator: Poedit 1.5.5\n"
"X-Poedit-SearchPath-0: /Users/mondre/Sites/wordpress-beta/wp-content/themes/"
"helm\n"

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/404.php:13
msgid "404 Page not found"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/404.php:17
msgid "Try searching..."
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/archive.php:13
#, php-format
msgid "Daily Archives: <span>%s</span>"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/archive.php:15
#, php-format
msgid "Monthly Archives: <span>%s</span>"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/archive.php:17
#, php-format
msgid "Yearly Archives: <span>%s</span>"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/archive.php:19
msgid "Author Archives: "
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/archive.php:21
msgid "Archive"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/category.php:8
#, php-format
msgid "Category : %s"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/comments.php:3
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/page-comments.php:3
msgid "Please do not load this page directly. Thanks!"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/comments.php:6
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/page-comments.php:6
msgid ""
"This post is password protected. Enter the password to view any comments."
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/comments.php:15
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/page-comments.php:14
msgid "No Responses"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/comments.php:15
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/page-comments.php:14
msgid "One Response"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/comments.php:15
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/page-comments.php:14
msgid "% Responses"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/header-navigation.php:47
msgid "Home"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/loop-attachment.php:34
msgid "Previous Image"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/loop-attachment.php:39
msgid "Lightbox"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/loop-attachment.php:44
msgid "Next Image"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/loop-attachment.php:86
msgid "Page"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/loop-page.php:15
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/template-fullwidth.php:22
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/template-page_leftsidebar.php:22
msgid "Pages:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/loop-page.php:17
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/template-contact.php:58
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/template-fullwidth.php:24
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/template-page_leftsidebar.php:24
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/template-sitemap.php:91
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/includes/postformats/post-contents.php:135
msgid "edit this entry"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/page-comments.php:50
msgid "Leave a Reply"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/page-comments.php:50
#, php-format
msgid "Leave a Reply to %s"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/search.php:15
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/search.php:28
#, php-format
msgid "Search Results for: %s"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/search.php:32
msgid "Nothing Found"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/search.php:33
msgid ""
"Sorry, but nothing matched your search criteria. Please try again with some "
"different keywords."
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/searchform.php:2
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/searchform.php:3
msgid "Search"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/tag.php:9
#, php-format
msgid "Tag : %s"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/template-sitemap.php:41
msgid "Authors"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/template-sitemap.php:53
msgid "Pages"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/template-sitemap.php:67
msgid "Posts"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/functions/framework-functions.php:110
msgid "Posted in "
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/functions/framework-functions.php:111
#, php-format
msgid ""
"<span class=\"sep\">Posted on </span><a href=\"%1$s\" title=\"%2$s\" rel="
"\"bookmark\"><time class=\"entry-date\" datetime=\"%3$s\" pubdate>%4$s</"
"time></a><span class=\"by-author\"> <span class=\"sep\"> by </span> <span "
"class=\"author vcard\"><a class=\"url fn n\" href=\"%5$s\" title=\"%6$s\" "
"rel=\"author\">%7$s</a></span></span>"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/functions/framework-functions.php:451
msgid "year"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/functions/framework-functions.php:452
msgid "month"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/functions/framework-functions.php:453
msgid "week"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/functions/framework-functions.php:454
msgid "day"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/functions/framework-functions.php:455
msgid "hour"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/functions/framework-functions.php:456
msgid "minute"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/functions/framework-functions.php:550
msgid "Page "
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/functions/framework-functions.php:550
msgid " of "
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/functions/framework-shortcodes.php:10
msgid "Top"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/options/options-data.php:246
msgid "Autoplay"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/options/options-data.php:247
msgid "Set autoplay"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/options/options-data.php:255
msgid "Transition style"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/options/options-data.php:256
msgid "Transition style for nivo"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/options/options-data.php:268
msgid "Slideshow speed"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/options/options-data.php:269
msgid "Set the speed of the slideshow cycling, in milliseconds"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/options/options-data.php:280
msgid "Animation duration"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/options/options-data.php:281
msgid "Set the speed of animations, in milliseconds"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/options/options-data.php:563
msgid "Portfolio Column Style"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/options/options-data.php:564
msgid "Portfolio column style"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/options/options-data.php:595
msgid "Link Images"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/options/options-data.php:596
msgid ""
"Display the linked images to either the lightbox or link directly to the "
"respective post or page."
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/options/options-data.php:597
msgid "How to display the images"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/options/admin/options-framework.php:135
msgid "Options Imported!"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/options/admin/options-framework.php:166
msgid "Invalid Import"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/options/admin/options-framework.php:168
msgid "Options Saved!"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/options/admin/options-framework.php:249
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/options/admin/options-framework.php:463
msgid "Default options restored."
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/options/admin/options-framework.php:429
msgid "Click OK to reset. Any theme settings will be lost!"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/options/admin/options-framework.php:504
msgid "Options saved."
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/options/admin/options-framework.php:563
msgid "Theme Options"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/options/admin/options-medialibrary-uploader.php:26
msgid "Options Framework Internal Container"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/options/admin/options-medialibrary-uploader.php:128
msgid "Upload"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/options/admin/options-medialibrary-uploader.php:151
msgid "View File"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/options/admin/options-medialibrary-uploader.php:288
msgid "Gallery"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/framework/options/admin/options-medialibrary-uploader.php:288
msgid "Previously Uploaded"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/custom-post-types.php:17
msgid "Portfolio List"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/custom-post-types.php:18
msgid "Portfolio"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/custom-post-types.php:36
msgid "Featured List"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/custom-post-types.php:37
msgid "Manage your Featured posts to display on the Mainpage"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/custom-post-types.php:38
msgid "Featured"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/custom-post-types.php:95
msgid "Caption:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/custom-post-types.php:97
msgid "Big title:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/custom-post-types.php:99
msgid "Link to:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/custom-post-types.php:108
msgid "Featured Title"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/custom-post-types.php:109
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/custom-post-types.php:294
msgid "Description"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/custom-post-types.php:110
msgid "Linked to"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/custom-post-types.php:111
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/custom-post-types.php:297
msgid "Image"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/custom-post-types.php:189
msgid "Thumbnail gallery options"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/custom-post-types.php:191
msgid "Description:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/custom-post-types.php:195
msgid "Client ( optional )"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/custom-post-types.php:203
msgid "Project Link ( optional )"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/custom-post-types.php:209
msgid "Optional Thumbnail:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/custom-post-types.php:210
msgid ""
"Please provide optional thumbnail image path for your portfolio thumbnails. "
"If empty the featured image attached will be used to show the thumbnail. <br/"
">"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/custom-post-types.php:214
msgid "Video URL:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/custom-post-types.php:215
msgid ""
"Eg.<br/>http://www.youtube.com/watch?v=D78TYCEG4<br/>http://vimeo."
"com/172881<br/>http://www.adobe.com/products/flashplayer/include/marquee/"
"design.swf?width=792&height=294"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/custom-post-types.php:221
msgid "Link to ( optional )"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/custom-post-types.php:227
msgid "Portfolio details page options"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/custom-post-types.php:244
msgid "Video Embed Code"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/custom-post-types.php:293
msgid "Portfolio Title"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/custom-post-types.php:295
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/video.php:7
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/video.php:13
msgid "Video"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/custom-post-types.php:296
msgid "Types"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/custom-post-types.php:322
msgid "Image not found"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/theme-functions.php:55
msgid "Posted in: "
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/theme-functions.php:66
msgid "Tags: "
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/theme-functions.php:67
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/includes/postformats/post-contents.php:140
msgid "This entry was posted on"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/theme-functions.php:68
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/includes/postformats/post-contents.php:141
msgid "You can follow any responses to this entry through the"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/theme-functions.php:68
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/includes/postformats/post-contents.php:141
msgid "feed."
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/shortcodegens/accordiontabs/window.php:13
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/shortcodegens/buttons/window.php:13
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/shortcodegens/columns/window.php:13
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/shortcodegens/dividers/window.php:13
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/shortcodegens/gmaps/window.php:13
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/shortcodegens/icons/window.php:13
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/shortcodegens/notices/window.php:13
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/shortcodegens/picture/window.php:13
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/shortcodegens/postpages/window.php:13
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/shortcodegens/slideshow/window.php:13
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/shortcodegens/textboxes/window.php:13
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/shortcodegens/thumbnails/window.php:13
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/shortcodegens/type/window.php:13
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/functions/shortcodegens/videos/window.php:13
msgid "You are not allowed to be here"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/includes/post-related.php:19
msgid "Related Posts"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/includes/portfolio/ajax-loader.php:40
msgid "Project Link"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/includes/portfolio/portfolio-ajax-gallery.php:23
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/includes/portfolio/portfolio-filterable.php:11
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/includes/portfolio/portfolio-filterable.php:14
msgid "All"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/includes/portfolio/portfolio-common-types.php:24
#, php-format
msgid "View portfolios in %s"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/includes/portfolio/portfolio-filterable.php:11
msgid "Displaying"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/includes/postformats/post-contents.php:142
msgid "<p>Tags: "
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/includes/postformats/post-contents.php:143
msgid "Posted in:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/includes/postformats/post-data.php:13
msgid "ago"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/address.php:5
msgid "Display your contact details"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/address.php:6
msgid "Address"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/address.php:12
msgid "Contact Us"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/address.php:84
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/flickr.php:51
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/popular.php:135
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/recent.php:133
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/sidebar-gallery.php:346
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/twitter.php:79
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/video.php:51
msgid "Title:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/address.php:87
msgid "Intro text:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/address.php:90
msgid "Company:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/address.php:93
msgid "Address:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/address.php:96
msgid "City:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/address.php:99
msgid "State:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/address.php:102
msgid "Zip:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/address.php:105
msgid "Phone:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/address.php:108
msgid "Cell phone:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/address.php:111
msgid "e-mail:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/flickr.php:6
msgid "Displays photos from a Flickr ID"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/flickr.php:7
msgid "Flickr"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/flickr.php:12
msgid "On Flickr"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/flickr.php:54
msgid ""
"Flickr ID (<a href=\"http://www.idgettr.com\" target=\"_blank\">idGettr</a>):"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/flickr.php:57
msgid "Number of photo to display:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/flickr.php:60
msgid "Display type:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/flickr.php:62
msgid "Latest"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/flickr.php:63
msgid "Random"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/popular.php:5
msgid "Displays the popular posts"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/popular.php:6
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/popular.php:28
msgid "Popular Posts"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/popular.php:138
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/recent.php:136
msgid "Posts to Display:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/popular.php:141
msgid "Description Length:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/popular.php:145
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/recent.php:143
msgid "Select from Categories:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/recent.php:5
msgid "Displays the recent posts"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/recent.php:6
#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/recent.php:28
msgid "Recent Posts"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/recent.php:139
msgid "Description Lenght:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/sidebar-gallery.php:353
msgid "List Choice:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/sidebar-gallery.php:365
msgid ""
"Select Page <br/><small><strong>If List-Choice is Page</strong></small> :"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/sidebar-gallery.php:375
msgid ""
"Select Category <br/><small><strong>If List-Choice is Category</strong></"
"small> :"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/sidebar-gallery.php:385
msgid ""
"Select Portfolio <br/><small><strong>If List-Choice is Portfolio</strong></"
"small> :"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/sidebar-gallery.php:395
msgid "Click Action:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/sidebar-gallery.php:406
msgid "Image Order:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/sidebar-gallery.php:417
msgid "Limit Portfolio:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/social.php:4
msgid "Generate social icons"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/social.php:6
msgid " - Social Widget"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/social.php:132
msgid "Follow Us!"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/social.php:134
msgid "Follow Us on"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/social.php:169
msgid "Icon Size"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/social.php:179
msgid "Type of Animation"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/social.php:190
msgid "Open in new tab?"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/social.php:202
msgid "Contact text:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/social.php:207
msgid "Contact text link:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/social.php:219
msgid "Social Name:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/social.php:221
msgid "Social Icon URL:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/social.php:223
msgid "Social Profile URL:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/twitter.php:82
msgid "Username"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/twitter.php:86
msgid "Avatar Status:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/twitter.php:94
msgid "No. of Feeds:"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/video.php:5
msgid "Video embed widget"
msgstr ""

#: /Users/mondre/Sites/wordpress-beta/wp-content/themes/helm/widgets/video.php:54
msgid "Video Embed Code:"
msgstr ""
